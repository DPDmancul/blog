.PHONY: build serve tags clean

TAGS_PATH := content/tags
HUGO = hugo $(1) || nix-shell --run "hugo $(1)"

build: tags
	$(call HUGO)

serve: tags
	$(call HUGO,server)

tags:
	@chmod +x tags.sh
	./tags.sh

clean:
	rm -rf $(TAGS_PATH)
	rm -rf public
