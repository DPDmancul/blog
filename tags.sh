#! /usr/bin/env bash

tag() {
  TAGPATH="content/tags/${1// /-}"
  mkdir -p "$TAGPATH"
  printf -- '---\ntitle: "%s"\n---' "$1" > "$TAGPATH/_index.md"
  printf -- '---\ntitle: "%s"\n---' "$2" > "$TAGPATH/_index.it.md"
}

tag agda agda
tag computation computabilità
tag concepts idee
tag do-it-yourself "fai da te"
tag design design
tag economy economia
tag electronics elettronica
tag estimate stima
tag faith fede
tag life vita
tag linguistics linguistica
tag logic logica
tag "machine learning" "machine learning"
tag math matematica
tag money soldi
tag philosophy filosofia
tag probability probabilità
tag religion religione
tag "type theory" "teoria dei tipi"
