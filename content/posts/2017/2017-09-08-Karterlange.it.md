---
title: Karterlange
author: Davide Peressoni
cover: https://gitlab.com/uploads/-/system/project/avatar/2164022/Karter.svg.png
tags: [linguistics, concepts]
description: Una lingua in fase di sviluppo
---

[Grammatica](https://gitlab.com/DPDmancul/Karterlange_it/-/wikis/home)

[Dizionario](https://dakation.altervista.org/Karter/page/diz.php)
