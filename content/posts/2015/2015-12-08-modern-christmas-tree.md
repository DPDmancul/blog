---
title: Modern wood Christmas tree
author: Davide Peressoni
cover: https://cdn.thingiverse.com/renders/ae/60/45/91/58/P_20151209_210330_1_HDR_p_display_large.jpg
tags: [do-it-yourself, design, concepts]
# description:
---

A modern wood Christmas Tree: it can be build in 4 parts of plywood, linked each others with a little chain.

Link to project: <https://www.thingiverse.com/thing:1183722>

![](https://cdn.thingiverse.com/renders/3b/19/25/9e/5d/misure_display_large.jpeg)
