---
title: I soldi riescono a rappresentare la nostra società?
slug: soldi-società
author: Davide Peressoni
# cover:
tags: [economy, money, life, philosophy]
# description:
---

Il sistema economico globale che usiamo oggi è frutto delle varie modifiche che ha subito nel corso della storia il primo sistema economico: il baratto.  
Il baratto ha una logica molto semplice:  io ti do una cosa e in cambio tu mi dai un'altra cosa dello stesso valore; oggi la situazione non è molto diversa, solo che usiamo un intermediario per i baratti, i soldi: io ti do una cosa e in cambio tu mi dai un'equivalente in denaro.

Questo sistema però da la possibilità di fare anche scambi non naturali: il primo esempio è il brevetto/diritto d'autore coi quali si scambia un'idea con dei soldi, ma se pensiamo all'equivalente col baratto si può dire che se io ho una mela e tu hai una pera e ce le scambiamo entrambi abbiamo un frutto, ma se io ho un'idea e tu hai un'idea diversa e ce le scambiamo, entrambi abbiamo due idee, allora se io ho un'idea e tu dei soldi e ce li scambiamo, io ho dei soldi e l'idea, mentre tu avrai solo l'idea. Lo stesso vale per tutto ciò che offre un servizio (settore terziario) nonché per tutte le opere digitali (basta un copia-incolla per distribuirle, senza costi).  
Siamo tutti concordi che è giusto retribuire i lavoratori per il tempo e l'ingegno spesi nell'ideare, nel fare, e nel creare, ma possiamo altrettanto facilmente accorgerci che i soldi non sono adatti a descrivere correttamente queste attività, che sono le principali ai nostri giorni.  
Un secondo esempio di innaturalità è la finanza: nelle borse ci sono persone che impazziscono per riuscire a vendere e comprare titoli in modo da guadagnarci sopra.  
Ma cosa sono i titoli? i titoli sono delle quote di società o di stato, che di persè non valgono molto, ma se comprate e poi vendute al momento giusto comportano alti guadagni; il problema è che il momento giusto dura qualche millisecondo: oggi queste compravendite sono gestite da computer e in pochi secondi società possono fallire o aver successo.  
Quindi questo, che era nato come un sistema per finanziare le società che lavoravano meglio, si è trasformato in un comprare soldi coi soldi.  
Il problema più grande è che questa compravendita innaturaleostacola la gente comune, ma fornendo grandi guadagni ha attirato l'attenzione delle banche che ormai non sono più per il cittadino, ma per la finanza.  
Questo sistema «Arricchisce ulteriormente i ricchi e abbandona i poveri al loro destino»[^1] creando così un divario che è costretto a crescere sempre di più, il quale genera così la crisi perché la maggior parte della popolazione ha la minor parte delle ricchezze e la minor parte della popolazione ha la maggior parte ha la maggior parte delle ricchezze: la crisi non è la mancanza di soldi (impossibile) ma la loro mal distribuzione.

[^1]: Luigi Zingales, _Manifesto Capitalista. Una rivoluzione liberale contro un'economica corrotta_, Rizzoli, Milano 2012

Questo crea un blocco, accentuato anche dai debiti che abbiamo: viviamo in «un sistema nel quale i debiti non si rimborsano mai»[^2] perché pur teoricamente avendo i soldi per saziarli, in realtà, avendoli a nostra volta prestati o investiti, non ce li abbiamo.

[^2]: Giorgio Ruffolo e Stefano Sylos Labini, _Il film della crisi_, Einaudi, Torino 2012

Quali sono quindi i metodi per uscire dalla crisi? Certamente servono la concorrenza, gli investimenti, il mercato globale, ... ma sopratutto un maggior interesse delle banche verso i cittadini e una rete di sostegno fornita dalla stato, è infatti essenziale avere delle garanzie come l'assistenza medica, le pensioni, sostegni economici, ... il tutto corredato da accordi globali: se viviamo in un mercato globale dobbiamo vivere in uno stato globale, non possiamo infatti avere migliaia di regole per la stessa cosa.

Ora attraverso le nuove comunicazioni l'idea di stato e di nazione sta un po' scomparendo: comunichiamo con tutto il mondo, compriamo da tutto il mondo, ... insomma, siamo cittadini del mondo!

«Una buona economia non può essere separata da una buona politica»[^3], infatti diritti, prezzi, leggi, partecipazione devono sempre andare a braccetto ed evolvere assieme, non arrancando per rimanere al passo con quelle più arretrate.

[^3]: Raghuram G. Rajan, _Terremoti finanziari_, Einaudi, Torino 2012

Comunque  c'è da ricordarsi che il sistema economico globale non è l'unico che esiste al giorno d'oggi: alcuni popoli usano ancora il baratto o addirittura dei sistemi "di fiducia" in cui la gente non chiede niente in cambio di quello che offre perché sa che gli verrà dato altrettanto, sistema da cui potremmo prendere spunto per migliorare.
