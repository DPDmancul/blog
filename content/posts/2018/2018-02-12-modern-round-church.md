---
title: Concept for modern round church
author: Davide Peressoni
cover: https://cdn.thingiverse.com/renders/b5/64/1d/7d/e3/e81fe12877287cd71ec1dc237a96195c_display_large.jpg
tags: [design, concepts, faith, religion]
# description:
---

Link to project: <https://www.thingiverse.com/thing:2791347>

> "Our Father who are in heaven" (Mt 6:9-13)

We are all siblings each other: being all in a circle makes us feel united.

![All in circle](https://cdn.thingiverse.com/renders/e7/3a/5a/4d/14/92f491497103c372d64d5575121f9092_display_large.jpg)

The tabernacle, with the body of Christ, is in the center of the circle; so all are turned towards Jesus, not only the people.


![The tabernacle](https://cdn.thingiverse.com/renders/e1/f9/dd/f1/58/9c5a80b4f82411e0474fd380c432e137_display_large.jpg)

The tabernacle is the top of a tree with the roots in Heaven. It contains Jesus, who brings Heaven on earth.

Usually the tabernacle is lifted up, in order to be able to see from one side of the circle to the other. However, it is always clearly visible.

On the holy communion, the tabernacle is lifted down, to allow the minister access it.

![holy communion](https://cdn.thingiverse.com/renders/8d/23/00/ba/29/e0b02568efb056d95b6edbce84de0165_display_large.jpg)
