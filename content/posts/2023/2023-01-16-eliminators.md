---
title: Understanding eliminators
author: Davide Peressoni
# cover:
tags: [type-theory, math, logic, agda, computation]
# description:
math: true
---

Eliminators are undoubtedly the most difficult aspect of [type theory](https://en.wikipedia.org/wiki/Type_theory). In this article we will try to make more clear how they work, but most importantly why we need them. This articles will present some analogies between eliminators and constructs of programming languages (such Python, Rust, Scala, ...) which can help who knows some basic of computer programming understanding eliminators. Agda will be used to implement all eliminators, since it is a language (and proof assistant) built on top of [Martin-Löf's type theory](https://en.wikipedia.org/wiki/Intuitionistic_type_theory). Don't worry if you don't know how to develop: the only prerequisite is to know how to define a math function by cases, which all of us studied at school.

## Type theory

Before starting our journey, let's do a brief recap of what is type theory. Type theory is a math field which aims to:

- Solve some issues in lambda-calculus (a computation model) by adding types to variables
- Provide a small set of axioms to prove foundations of mathematics: the less we postulate, the more we can prove, the more we can trust

For this last point, type theory is used as a basis for many proof assistants. A proof assistant (such [Agda](https://en.wikipedia.org/wiki/Agda_(programming_language)), [Coq](https://en.wikipedia.org/wiki/Coq), [Idris](https://en.wikipedia.org/wiki/Idris_(programming_language)), [Isabelle](https://en.wikipedia.org/wiki/Isabelle_(proof_assistant)), ...) is both a programming language (usually functional) and an environment to help producing and validating mathematical proofs and algorithms correctness ones.

## Eliminators purpose
Before talking about what is an eliminator and how it works, let's think about why we need eliminators. Let's take an overview of which are the instruments type theory give us.

- **Formation rule**  
  With the formation rule we introduce a type: we state that a type exists, under a certain context. The context is useful to fix preconditions, hypothesis on the variables used in the definition of the type. Even if context is directly useful only for dependent types, we always put a generic context, also in formation rules of non-dependent types: this way we can always restrict the context and chain rules.  

  \begin{prooftree}
    \AxiomC{$\Gamma$ context}
    \LeftLabel{F-$\mathsf{Unit}$)}
    \UnaryInfC{$\mathsf{Unit}$ type [$\Gamma$]}
  \end{prooftree}

  The above says that given a generic context $\Gamma$, $\mathsf{Unit}$ is a type. For who has not seen this notation before, you can read the "fraction" as an implication: above the are the premises and below the consequences. On the left we have a superfluous label.

  \begin{prooftree}
    \AxiomC{$A$ type [$\Gamma$]}
    \LeftLabel{F-$\mathsf{List}$)}
    \UnaryInfC{$\mathsf{List}(A)$ type [$\Gamma$]}
  \end{prooftree}

  Here we see a dependent type definition: for each type $A$ under context $\Gamma$, $\mathsf{List}(A)$ is a type, under the same context. As we can see here we have a precondition.

- **Introduction rules**  
  An empty type is not very useful (except the _empty type_ itself), so we need to introduce members of types. These constructors are defined in introduction rules, which simply state that, under a context, a certain object is member of a certain type.

  \begin{prooftree}
    \AxiomC{$\Gamma$ context}
    \LeftLabel{I-$\mathsf{Unit}$)}
    \UnaryInfC{tt $\in \mathsf{Unit}$ [$\Gamma$]}
  \end{prooftree}

  The unique constructor of the unit type is $\mathrm{tt}$. Its introduction rule tells us that given a generic context $\Gamma$, $\mathrm{tt}$ is an element of the unit type.

With those two tools we can create variables of a certain type, but we cannot use them to do computations. We are missing the basic operations. If we were using classical natural numbers we could operate on them with their basic operations (i.e. addition, multiplication, ...) and easily write functions, e.g. $\lambda(a,b).a+b$.[^lambda] But we don't have such operators defined for our types in type theory.

[^lambda]: For those not adept to [lambda calculus](https://en.wikipedia.org/wiki/Lambda_calculus) $\lambda(a,b).a+b$ (or more formaly $\lambda(a).\lambda(b).a+b$) is a definition of a function which takes two parameters ($a$ and $b$) and returns their sum. It is somewhat equivalent to $(a,b) \mapsto a+b$, or $f(a,b)=a+b$.

The objective of eliminators is allowing us to define functions to do computations with the variables of our types. More precisely eliminators are functions which take a variable of some type and give us a result, based on the value of the variable we gave to it. We can so compare our eliminator as a [`switch`](https://en.wikipedia.org/wiki/Switch_statement) (in C, C++, C#, Java, ...), [`match`](https://en.wikipedia.org/wiki/Pattern_matching) (in Python, Rust, Scala, ...) or `case` (in Haskell, Agda, ...).[^match] Indeed defining a function using eliminators will be like defining a function by cases.

[^match]: As we will see `match` from Rust and Scala, and `case` from Haskell and Agda are indeed an accurate implementation of eliminators.

## How eliminators work

As we said before, eliminators allow us to define functions with variables of our type, defining them by cases.
In general terms an eliminator has $n+1$ parameters, where $n$ is the number of constructors of the type. The first parameter is the variable we want to match to, the others are the values we would like to return if we match a certain constructor.

$$\mathit{El}(x, y_1, y_2, \dots, y_n) = \begin{cases}
  y_1 & \text{if }x = \text{constuctor 1}\\\\
  y_2 & \text{if }x = \text{constuctor 2}\\\\
  \dots\\\\
  y_n & \text{if }x = \text{constuctor $n$}\\\\
\end{cases}$$

Let's now see some examples to understand better

### Singleton

As said before, the singleton, or [unit type](https://en.wikipedia.org/wiki/Unit_type), has only one constructor ($\mathrm{tt}$).
So singleton eliminator will have only two arguments: the variable to match, and wath we will return if we match that constructor.

$$\mathit{El_{\mathsf{Unit}}}(x, y) = \begin{cases}y & \text{if }x = \mathrm{tt}\end{cases}$$

We can write that in Python as

```python
class Unit(Enum):
    tt = 1

    def eliminator(self, y):
        match self:
            case Unit.tt:
                return y
```

We have defined the unit type as an [enumeration](https://en.wikipedia.org/wiki/Enumerated_type) with only one value (`tt`). Then we have added the `eliminator` method, which matches on `self` for all possible constructor (in this case only `tt`) and returns the appropriate value (`y`).

Obviously the variable $y$ could be of whatever type, this is highlighted by the usage of generics in Rust:[^rust-unit]

[^rust-unit]: Rust already have a singleton type `()`, whose only value is `()`. We declared an enum to see better how the eliminator behaves.

```rust
enum Unit {
    Tt
}

impl Unit {
    fn eliminator<T>(self, y: T) -> T {
        match self {
            Unit::Tt => y
        }
    }
}
```

Paying attention at the definition of the eliminator we can see the type $T$ of the variable $y$ could be a dependent type on the variable we are matching on.

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{Unit}$]}
    \AxiomC{$x\in\mathsf{Unit}$ [$\Gamma$]}
      \AxiomC{$y\in T(\mathrm{tt})$ [$\Gamma$]}
  \LeftLabel{E-$\mathsf{Unit}$)}
  \TrinaryInfC{$El_{\mathsf{Unit}}(x,y)\in T(x)$ [$\Gamma$]}
\end{prooftree}

This dependency could potentially be useful, in types with multiple constructors, to have multiple return types based on the input.

Python and Rust do not support dependent types, so here we will see our eliminator implemented in Agda:

```agda
data Unit : Set where
  tt : Unit

ElUnit : {T : Unit → Set} → (x : Unit) → T tt → T x
ElUnit tt y = y
```

Here we clearly state that our generic type `T` is a dependent type on a value of type `Unit` (recall in the context we have fixed $z\in\mathsf{Unit}$). `y` is of type $T(\mathrm{tt})$, and the return value of type $T(x)$. In this case this is not important since the unique value of type `Unit` is `tt`, but we will later see how this is still useful when chaining eliminators together.

We can rewrite the Agda code to compare it better with the eliminator and conversion rules.[^conversion-rule]

[^conversion-rule]: Eliminators are defined by two rules: the eliminator rule declare the return type of the eliminator, the conversion rules instead define the actual case split.

```agda
ElUnit : {T : (z : Unit) → Set} →  -- T(z) type [z ∈ Unit]
         (x : Unit) →              -- x ∈ Unit
         (y : T tt) →              -- y ∈ T(tt) 
         T x                       -- ElUnit(x,y) ∈ T(x)
ElUnit tt y = y                    -- ElUnit(tt,y) = y
```

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{Unit}$]}
    \AxiomC{$y\in T(\mathrm{tt})$ [$\Gamma$]}
  \LeftLabel{C-$\mathsf{Unit}$)}
  \BinaryInfC{$El_{\mathsf{Unit}}(\mathrm{tt},y) = y \in T(\mathrm{tt})$ [$\Gamma$]}
\end{prooftree}

This example is great to start, but it is not really interesting because splitting on a single constructor makes no practical sense. So let's add some fun trying with two constructors.

### Booleans

The bool type represent truth values (`true` and `false`). It's not a canonical type, since it could be constructed from canonical types[^bool], but we will use it for its simplicity.

[^bool]: In facts the type $\textsf{Unit}+\textsf{Unit}$ (disjoint sum of two units) is used to represent boolean data.

\begin{prooftree}
  \AxiomC{$\Gamma$ context}
  \LeftLabel{F-$\textsf{Bool}$)}
  \UnaryInfC{$\textsf{Bool}$ type [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$\Gamma$ context}
  \LeftLabel{I$_1$-$\textsf{Bool}$)}
  \UnaryInfC{true $\in\textsf{Bool}$ [$\Gamma$]}
  \AxiomC{$\Gamma$ context}
  \LeftLabel{$\quad$I$_2$-$\textsf{Bool}$)}
  \UnaryInfC{false $\in\textsf{Bool}$ [$\Gamma$]}
\end{prooftree}

The eliminator, intuitively, would be:

$$
  El_{\mathsf{Bool}}(x,y_1,y_2) = \begin{cases}
    y_1 & \text{if }x = \mathrm{true} \\\\
    y_2 & \text{if }x = \mathrm{false} \\\\
  \end{cases}
$$

This seem more interesting than the one of singleton. Computer scientists could also have recognized how this eliminator in no more less than the `if` construct.

```pseudo
function ElBool(x, y1, y2)
│ if (x)       // x is true
│ │ return y1
│ else         // x is false
│ │ return y2
│ └
└
```

Before implementing the eliminator, let's give a formal definition of it:

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{Bool}$]}
  \noLine
  \UnaryInfC{$y_1\in T(\mathrm{true})$ [$\Gamma$]}
    \AxiomC{$x\in\mathsf{Bool}$ [$\Gamma$]}
    \noLine
    \UnaryInfC{$y_2\in T(\mathrm{false})$ [$\Gamma$]}
  \LeftLabel{E-$\mathsf{Bool}$)}
  \BinaryInfC{$El_{\mathsf{Bool}}(x,y_1,y_2)\in T(x)$ [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{Bool}$]}
    \AxiomC{$y_1\in T(\mathrm{true})$ [$\Gamma$]}
      \AxiomC{$y_2\in T(\mathrm{false})$ [$\Gamma$]}
  \LeftLabel{C$_1$-$\mathsf{Bool}$)}
  \TrinaryInfC{$El\_{\mathsf{Bool}}(\mathrm{true},y_1,y_2) = y_1\in T(\mathrm{true})$ [$\Gamma$]}
\end{prooftree}
\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{Bool}$]}
    \AxiomC{$y_1\in T(\mathrm{true})$ [$\Gamma$]}
      \AxiomC{$y_2\in T(\mathrm{false})$ [$\Gamma$]}
  \LeftLabel{C$_2$-$\mathsf{Bool}$)}
  \TrinaryInfC{$El\_{\mathsf{Bool}}(\mathrm{false},y_1,y_2) = y_2\in T(\mathrm{false})$ [$\Gamma$]}
\end{prooftree}

As we can see, this time the resulting type can be really different, based on the input: if $x=\mathrm{true}$, the result would be of type $T(\mathrm{true})$, instead with $x=\mathrm{false}$ the result type would be $T(\mathrm{false})$.

```python
Ttrue = TypeVar('Ttrue')
Tfalse = TypeVar('Tfalse')

class Bool(Enum):
    true = 1
    false = 2

    #              Bool      T(true)    T(false)   T(x)
    def eliminator(self, y1: Ttrue, y2: Tfalse) -> Union[Ttrue, Tfalse]:
        match self:
            case Bool.true:
                return y1
            case Bool.false:
                return y2
```

Python does not have dependent types: we declared two generics (`Ttrue` and `Tfalse`) and we declared a return type of `Union[Ttrue, Tfalse]`, i.e. one of the two types. This simulates well our dependent type $T(x)$.

```agda
data Bool : Set where
  true  : Bool
  false : Bool

ElBool : {T  : (z : Bool) → Set} →  -- T(z) type [z ∈ Bool]
         (x  : Bool) →              -- x ∈ Bool
         (y1 : T true) →            -- y1 ∈ T(true) 
         (y2 : T false) →           -- y2 ∈ T(false) 
         T x                        -- ElBool(x,y1,y2) ∈ T(x)
ElBool true  y1 y2 = y1             -- ElBool(true,y1,y2) = y1
ElBool false y1 y2 = y2             -- ElBool(false,y1,y2) = y2
```

With Agda dependent types, we can better understand how the bool eliminator works. In fact we can predict the output type from the value of the parameter $x$.

### Naturals

It would be impossible to have a constructor for each natural number: they are infinite. Instead we define natural numbers recursively.

\begin{prooftree}
  \AxiomC{$\Gamma$ context}
  \LeftLabel{F-$\textsf{Nat}$)}
  \UnaryInfC{$\textsf{Nat}$ type [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$\Gamma$ context}
  \LeftLabel{I$_1$-$\textsf{Nat}$)}
  \UnaryInfC{zero $\in\textsf{Nat}$ [$\Gamma$]}
  \AxiomC{$m\in\mathsf{Nat}$ [$\Gamma$]}
  \LeftLabel{$\quad$I$_2$-$\textsf{Nat}$)}
  \UnaryInfC{suc($m$) $\in\textsf{Nat}$ [$\Gamma$]}
\end{prooftree}

We have a base constructor (zero), which represents the number 0, and then a constructor (suc) which transforms a natural into its successor. This constructors take a natural number and returns a new natural.

Following the same reasoning of above the eliminator would be:

$$
  El_{\mathsf{Nat}}(n,y_1,y_2) = \begin{cases}
    y_1 & \text{if }n = \mathrm{zero} \\\\
    y_2 & \text{if }n = \mathrm{suc}(m)
  \end{cases}
$$

But such an eliminator wouldn't be useful. For example how are we supposed to define the sum between two numbers using it?

$$
  \mathrm{sum}(a,b):=El_{\mathsf{Nat}}(a,?_1,?_2) = \begin{cases}
    ?_1 & \text{if }a = \mathrm{zero} \\\\
    ?_2 & \text{if }a = \mathrm{suc}(a')
  \end{cases}
$$

The first hole could be filled with $b$ (since $a$ is zero and $0+b=b$), but what can we put in the second hole? Here $a = \mathrm{suc}(a')=$ $1+a'$ and so we have to return $a+b=$ $(1+a')+b=$ $1+(a'+b)=$ $\mathrm{suc}(a'+b)=$ $\mathrm{suc}(\mathrm{sum}(a',b))$, but unfortunately we cannot obtain $a'$:

$$
  \mathrm{sum}(a,b):=El_{\mathsf{Nat}}(a,b,\mathrm{suc}(\mathrm{sum}({\color{red}a'},b)))
$$

We can easily fix it by substituting the parameter $y_2$: instead to be a variable we can make it a function. This function takes a natural ($m$) and returns what would have been $y_2$:

$$
  El_{\mathsf{Nat}}(n,y,f) = \begin{cases}
    y & \text{if }n = \mathrm{zero} \\\\
    f(m) & \text{if }n = \mathrm{suc}(m)
  \end{cases}
$$
$$
  \mathrm{sum}(a,b):=El_{\mathsf{Nat}}(a,b,\lambda(a').\mathrm{suc}(\mathrm{sum}(a',b)))
$$

We didn't talk about the recursive call to the sum function: sum is defined with a call to sum itself. This makes sum a recursive function. It is guaranteed to terminate since each time a recursive call is done one parameter (in this case the first) becomes smaller (in this case we remove a $\mathrm{suc}$ wrapping, or equivalently we decrease it by 1), finally reaching the base constructor (which implies being in the base case of the function).

In type theory we often use recursion to define types, and thus to define functions to operate on them. Hence the canonical eliminators already give us the result of this recursive call, simplifying the definition of other functions:

$$
  El_{\mathsf{Nat}}(n,y,e) = \begin{cases}
    y & \text{if }n = \mathrm{zero} \\\\
    e(m, El_{\mathsf{Nat}}(m,y,e)) & \text{if }n = \mathrm{suc}(m)
  \end{cases}
$$

As we can see, this time the function we pass takes not only the unwrapped value ($m$), but also the result of the recursive call of the eliminator. We can so simplify the definition of the sum function as:

$$
  \mathrm{sum}(a,b):=El_{\mathsf{Nat}}(a,b,\underbrace{\lambda(a', s).\mathrm{suc}(s)}_e)
$$

Recall that $s = El_{\mathsf{Nat}}(a',b,e)=$ $\mathrm{sum}(a',b)$.

<!--
```rust
enum Nat {
    Zero,
    Suc(Box<Nat>),
}

impl Nat {
    fn eliminator<T>(&self, y: T, e: &impl Fn(&Nat, T) -> T) -> T {
        match self {
            Nat::Zero => y,
            Nat::Suc(m) => e.call((m, m.eliminator(y, e))),
        }
    }
}

impl Add for Nat {
    type Output = Nat;

    fn add(self, other: Nat) -> Nat {
        self.eliminator(
            // 0 + other = other
            other,
            // (1 + m) + other = 1 + (m + other)
            &|_m, m_plus_other| Nat::Suc(Box::new(m_plus_other)),
        )
    }
}
```
-->
In Scala we can write this eliminator as:
```scala
enum Nat:
  case Zero
  case Suc (m: Nat)

  def eliminator [T] (y: T, e: (Nat, T) => T): T = this match
    case Zero    => y
    case Suc (m) => e (m, m.eliminator (y, e))

  def + (that: Nat): Nat =
    this.eliminator (
      // 0 + that = that
      that,
      // (1 + m) + that = 1 + (m + that)
      (m, m_plus_that) => Nat.Suc (m_plus_that),
    )
```

We are missing the most important thing: the type. Nonetheless we are talking about type theory.

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{Nat}$]}
    \AxiomC{$n\in\mathsf{Nat}$ [$\Gamma$]}
    \noLine
  \BinaryInfC{$y\in T(\mathrm{zero})$ [$\Gamma$] $\quad$ $e(m,x)\in T(\mathrm{suc}(m))$ [$\Gamma$, $m\in\mathsf{Nat}$, $x\in T(m)$]}
  \LeftLabel{E-$\mathsf{Nat}$)}
  \UnaryInfC{$El_{\mathsf{Nat}}(n,y,e)\in T(n)$ [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{Nat}$]}
    \AxiomC{$y\in T(\mathrm{zero})$ [$\Gamma$]}
    \noLine
  \BinaryInfC{$e(m,x)\in T(\mathrm{suc}(m))$ [$\Gamma$, $m\in\mathsf{Nat}$, $x\in T(m)$]}
  \LeftLabel{C$_1$-$\mathsf{Nat}$)}
  \UnaryInfC{$El\_{\mathsf{Nat}}(\mathrm{zero},y,e) = y \in T(\mathrm{zero})$ [$\Gamma$]}
\end{prooftree}
\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{Nat}$]}
    \AxiomC{$y\in T(\mathrm{zero})$ [$\Gamma$]}
    \noLine
  \BinaryInfC{$m\in\mathsf{Nat}$ [$\Gamma$] $\quad$ $e(m,x)\in T(\mathrm{suc}(m))$ [$\Gamma$, $m\in\mathsf{Nat}$, $x\in T(m)$]}
  \LeftLabel{C$_2$-$\mathsf{Nat}$)}
  \UnaryInfC{$El\_{\mathsf{Nat}}(\mathrm{suc}(m),y,e) = e(m, El\_{\mathsf{Nat}}(m,y,e)) \in T(\mathrm{suc}(m))$ [$\Gamma$]}
\end{prooftree}

```agda
data Nat : Set where
  zero : Nat
  suc  : Nat → Nat

ElNat : {T : (z : Nat) → Set} →                    -- T(z) type [z ∈ Nat]
        (n : Nat) →                                -- n ∈ Nat
        (y : T zero) →                             -- y ∈ T(zero)
        (e : (m : Nat) → (x : T m) → T (suc m)) →  -- e(m,x) ∈ T(suc(m)) [m ∈ Nat, x ∈ T(m)] 
        T n                                        -- ElNat(n,y,e) ∈ T(n) 
ElNat zero    y _ = y                  -- ElNat(zero,y,e) = y
ElNat (suc m) y e = e m (ElNat m y e)  -- ElNat(suc(m),y,e) = e(m, ElNat(m, y, e))

--     a     b    a+b
--     ↓     ↓     ↓
sum : Nat → Nat → Nat
sum a b = ElNat a b (λ a' a'+b → suc a'+b)
--   value to  ↗  ↑    ↑    ↑     ↖ return value if a = suc a'
--  be splitted   |    |   sum-El a' b  
--                |   a = succ a'
--     return value if a is zero

-- T n = Nat, in fact we can make it explicit as:
-- sum a b = ElNat a {λ _ → Nat} b (λ a' a'+b → succ a'+b)
```


As we said in the introduction some languages such Rust, Scala, Haskell, Agda, ... natively support eliminators: 


```scala
  def + (that: Nat): Nat = this match
    case Zero    => that
    case Suc (m) => Nat.Suc (m + that)
```

```agda
--     a     b    a+b
--     ↓     ↓     ↓
sum : Nat → Nat → Nat
sum zero     b = b              -- return value if a is zero
sum (suc a') b = suc (sum a' b) -- return value if a = suc a'
```

### Lists

Lists are similar to natural numbers: we have the empty list (*nil*) and the *cons*, i.e. the addition of an element to a list. The added difficulty is that we have to track the type of the elements of the list.

\begin{prooftree}
  \AxiomC{$A$ type [$\Gamma$]}
  \LeftLabel{F-$\mathsf{List}$)}
  \UnaryInfC{$\mathsf{List}(A)$ type [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$\mathsf{List}(A)$ type [$\Gamma$]}
  \LeftLabel{I$_1$-$\mathsf{List}$)}
  \UnaryInfC{$\mathrm{nil}\in\mathsf{List}(A)$ [$\Gamma$]}
  \AxiomC{$s\in\mathsf{List}(A)$ [$\Gamma$]}
    \AxiomC{$a \in A$ [$\Gamma$]}
  \LeftLabel{$\quad$I$_2$-$\mathsf{List}$)}
  \BinaryInfC{$\mathrm{cons}(s,a)\in\mathsf{List}(A)$ [$\Gamma$]}
\end{prooftree}

Also the eliminator is similar to that of naturals number, but the passed function, other than the recursive call, takes two unwrapped parameters. These are the prefix list ($s$), called _head_, and the tail element ($a$):

$$
  El_{\mathsf{List}}(x,y,e) = \begin{cases}
    y & \text{if }x = \mathrm{nil} \\\\
    e(s, a, El_{\mathsf{List}}(s,y,e)) & \text{if }x = \mathrm{cons}(s,a)
  \end{cases}
$$

Don't forget the types!

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{List}(A)$]}
    \AxiomC{$x\in\mathsf{List}(A)$ [$\Gamma$]}
      \AxiomC{$y\in T(\mathrm{nil})$ [$\Gamma$]}
      \noLine
  \TrinaryInfC{$\quad$ $e(s,w,t)\in T(\mathrm{cons}(s,w))$ [$\Gamma$, $s\in\mathsf{List}(A)$, $w\in A$, $t\in T(s)$]}
  \LeftLabel{E-$\mathsf{List}$)}
  \UnaryInfC{$El_{\mathsf{List}}(x,y,e)\in T(x)$ [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{List}(A)$]}
    \AxiomC{$y\in T(\mathrm{nil})$ [$\Gamma$]}
    \noLine
  \BinaryInfC{$e(s,w,t)\in T(\mathrm{cons}(s,w))$ [$\Gamma$, $s\in\mathsf{List}(A)$, $w\in A$, $t\in T(s)$]}
  \LeftLabel{C$_1$-$\mathsf{List}$)}
  \UnaryInfC{$El\_{\mathsf{List}}(\mathrm{nil},y,e) = y \in T(\mathrm{nil})$ [$\Gamma$]}
\end{prooftree}
\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z\in\mathsf{List}(A)$]}
    \AxiomC{$y\in T(\mathrm{nil})$ [$\Gamma$]}
      \AxiomC{$s\in\mathsf{List}(A)$ [$\Gamma$]}
      \noLine
  \TrinaryInfC{$a \in A$ [$\Gamma$] $\quad$ $e(s,w,t)\in T(\mathrm{cons}(s,w))$ [$\Gamma$, $s\in\mathsf{List}(A)$, $w\in A$, $t\in T(s)$]}
  \LeftLabel{C$_2$-$\mathsf{List}$)}
  \UnaryInfC{$El\_{\mathsf{List}}(\mathrm{cons}(s,a),y,e) = e(s,a, El\_{\mathsf{List}}(s,y,e)) \in T(\mathrm{cons}(s,a))$ [$\Gamma$]}
\end{prooftree}

```agda
data List (A : Set) : Set where
  nil  : List A
  cons : List A → A → List A

ElList : {A : Set}
         {T : List A → Set} →                 -- T(z) type [z ∈ List(A)]
         (x : List A) →                       -- x ∈ List(A)
         (y : T nil) →                        -- y ∈ T(nil)
                                              -- e(s,w,t) ∈ T(cons(s,w)) [s ∈ List(A), w ∈ A, t ∈ T(s)]
         (e : (s : List A) → (w : A) → (t : T s) → T (cons s w)) →
         T x                                  -- ElList(x,y,e) ∈ T(x)
ElList nil        y _ = y                     -- ElList(nil,c,e) = y
ElList (cons s a) y e = e s a (ElList s y e)  -- ElList(cons(s,a),y,e) = e(s,a,ElList(s,y,e))
```

### Disjoint sum

The disjoint sum type (known also as _Either_) allow us to represent values of two different types, taking note of the type of the current value. For example a variable of type $\mathsf{Unit}+\mathsf{Nat}$ could store a unit or a natural, and it knows which one is stored.[^union]

[^union]: In some programming languages such Haskell, Rust, ... the disjoint type is implemented with a tagged union. In other languages (such C and derived) you can simulate this with and (untagged) union, but here you have to manually take note of the type of the current value.

\begin{prooftree}
  \AxiomC{$B$ type [$\Gamma$]}
    \AxiomC{$C$ type [$\Gamma$]}
  \LeftLabel{F-$+$)}
  \BinaryInfC{$B+C$ type [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$B$ type [$\Gamma$]}
    \AxiomC{$C$ type [$\Gamma$]}
      \AxiomC{$b\in B$ [$\Gamma$]}
  \LeftLabel{I$_1$-$+$)}
  \TrinaryInfC{$\mathrm{inl}(b) \in B+C$ type [$\Gamma$]}
\end{prooftree}
\begin{prooftree}
  \AxiomC{$B$ type [$\Gamma$]}
    \AxiomC{$C$ type [$\Gamma$]}
      \AxiomC{$c\in C$ [$\Gamma$]}
  \LeftLabel{I$_2$-$+$)}
  \TrinaryInfC{$\mathrm{inr}(c) \in B+C$ type [$\Gamma$]}
\end{prooftree}

As we can see we have two constructors: one for each type. So the eliminator will take two functions: one unwrapping the left type, the other unwrapping the right type.

$$
  El_+(x,e_1,e_2) = \begin{cases}
    e_1(x_1) & \text{if }x = \mathrm{inl}(x_1) \\\\
    e_2(x_2) & \text{if }x = \mathrm{inr}(x_2)
  \end{cases}
$$

As you may have noticed here we don't have the recursive call. This because the disjoint sum type is not recursively defined, and so the functions which operate on its values will not be recursive.

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z \in B+C$]}
  \noLine
  \UnaryInfC{$e_1(x_1) \in T(\mathrm{inl}(x_1))$ [$\Gamma$, $x_1 \in B$]}
    \AxiomC{$x \in B+C$ [$\Gamma$]}
    \noLine
    \UnaryInfC{$e_2(x_2) \in T(\mathrm{inr}(x_2))$ [$\Gamma$, $x_2 \in C$]}
  \LeftLabel{E-$+$)}
  \BinaryInfC{$El_+(x,e_1,e_2) \in T(x)$ [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z \in B+C$]}
  \noLine
  \UnaryInfC{$e_1(x_1) \in T(\mathrm{inl}(x_1))$ [$\Gamma$, $x_1 \in B$]}
    \AxiomC{$b \in B$ [$\Gamma$]}
    \noLine
    \UnaryInfC{$e_2(x_2) \in T(\mathrm{inr}(x_2))$ [$\Gamma$, $x_2 \in C$]}
  \LeftLabel{C$_1$-$+$)}
  \BinaryInfC{$El\_+(\mathrm{inl}(b),e_1,e_2) = e_1(b) \in T(\mathrm{inl}(b))$ [$\Gamma$]}
\end{prooftree}
\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z \in B+C$]}
  \noLine
  \UnaryInfC{$e_1(x_1) \in T(\mathrm{inl}(x_1))$ [$\Gamma$, $x_1 \in B$]}
    \AxiomC{$c \in C$ [$\Gamma$]}
    \noLine
    \UnaryInfC{$e_2(x_2) \in T(\mathrm{inr}(x_2))$ [$\Gamma$, $x_2 \in C$]}
  \LeftLabel{C$_2$-$+$)}
  \BinaryInfC{$El\_+(\mathrm{inr}(c),e_1,e_2) = e_2(c) \in T(\mathrm{inr}(c))$ [$\Gamma$]}
\end{prooftree}

```agda
data _+_ (B C : Set) : Set where
  inl : B → B + C
  inr : C → B + C

El+ : {B C : Set}
      {T : B + C → Set} →            -- T(z) type [z ∈ B+C]
      (x : B + C) →                  -- x ∈ B+C
      (e₁ : (x₁ : B) → T (inl x₁)) → -- e₁(x₁) ∈ T(inl(x₁)) [x₁ ∈ B]
      (e₂ : (x₂ : C) → T (inr x₂)) → -- e₂(x₂) ∈ T(inr(x₂)) [x₂ ∈ C]
      T x                            -- El+(x,e₁,e₂) ∈ T(x)
El+ (inl b) e₁ _  = e₁ b             -- El+(inl(b),e₁,e₂) = e₁(b) 
El+ (inr c) _  e₂ = e₂ c             -- El+(inr(c),e₁,e₂) = e₂(c)
```

A naïve example of how to apply this eliminator to define a function is the following. It mirrors a disjoint sum: it maps the left element to the right and viceversa. Obviously the input and output types are different, even if are both disjoint sums of the same two types, but in different order.

$$
  \mathrm{swap}(x) = \begin{cases}
    \mathrm{inr}(x_1) & \text{if }x=\mathrm{inl}(x_1) \\\\
    \mathrm{inl}(x_2) & \text{if }x=\mathrm{inr}(x_2)
  \end{cases} = El_+(x, \lambda(x_1).\mathrm{inr}(x_1), \lambda(x_2).\mathrm{inl}(x_2))
$$

```agda
swap : {B C : Set} → B + C → C + B
swap x = El+ x inr inl
```

### Empty type

The empty type is a type with no inhabitants. It represents the logic value of _false_, and as from false we can derive anything, also from values of the empty type (which don't exist) we can derive anything.

\begin{prooftree}
  \AxiomC{$\Gamma$ context}
  \LeftLabel{F-$\mathsf{Empty}$)}
  \UnaryInfC{$\mathsf{Empty}$ type [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$T(z)$ type [$\Gamma$, $z \in \mathsf{Empty}$]}
    \AxiomC{$x \in \mathsf{Empty}$ [$\Gamma$]}
  \LeftLabel{E-$\mathsf{Empty}$)}
  \BinaryInfC{$El\_\mathsf{Empty}(x) \in T(x)$ [$\Gamma$]}
\end{prooftree}

```agda
data Empty : Set where

ElEmpty : {T : (z : Empty) → Set} →  -- T(z) type [z ∈ Empty]
          (x : Empty) →              -- x ∈ Empty
          T x                        -- ElEmpty(x) ∈ T(x)
ElEmpty ()
```

Since there are no values of the empty type, there is nothing to match, and so we don't have any conversion rule.

$$
  El_{\mathsf{Empty}}(x) = \begin{cases}\end{cases}
$$

Side note: only few rustaceans know the Rust [never type `!`](https://doc.rust-lang.org/reference/types/never.html), which is in fact an empty type. It is usually placed as a type for functions which don't terminate.

```rust
fn eliminator<T>(x: !) -> T {
    match x {
    }
}
```

### Propositional equality

The type of propositional equality states if two values are the same. If the values are not equal it is like the empty type. If instead the values are equal it is like a singleton with a unique element: its only constructor. This constructor represent a proof of the equality.

\begin{prooftree}
  \AxiomC{$A$ type [$\Gamma$]}
    \AxiomC{$a \in A$ [$\Gamma$]}
      \AxiomC{$b \in A$ [$\Gamma$]}
  \LeftLabel{F-$\mathsf{Id}$)}
  \TrinaryInfC{$\mathsf{Id}(A,a,b)$ type [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$a \in A$ [$\Gamma$]}
  \LeftLabel{I-$\mathsf{Id}$)}
  \UnaryInfC{$\mathrm{id}(a) \in \mathsf{Id}(A,a,a)$ [$\Gamma$]}
\end{prooftree}

As we can see $\mathrm{id}(a)$ is the only constructor of the propositional equality type between $a$ and $a$ itself, for other equality types there is no constructor. So the eliminator would be as easy as the singleton and empty type ones.

$$
  El_{\mathsf{Id}}(x,y) = \begin{cases}
    y & \text{if }x = \mathrm{id}(a)
  \end{cases}
$$

Since many times it is useful to access the value of $a$, we redefine the eliminator making it accept a function.

$$
  El_{\mathsf{Id}}(x,e) = \begin{cases}
    e(a) & \text{if }x = \mathrm{id}(a)
  \end{cases}
$$

\begin{prooftree}
  \AxiomC{$T(z_1,z_2,z_3)$ type [$\Gamma$, $z_1,z_2 \in A$, $z_3 \in \mathsf{Id}(A,z_1,z_2)$]}
    \AxiomC{$a,b \in A$ [$\Gamma$]}
    \noLine
    \BinaryInfC{$x \in \mathsf{Id}(A,a,b)$ [$\Gamma$] $\quad$ $e(w) \in T(w,w,\mathrm{id}(w))$ [$\Gamma$, $w \in A$]}
  \LeftLabel{E-$\mathsf{Id}$)}
  \UnaryInfC{$El_{\mathsf{Id}}(x,e) \in T(a,b,x)$ [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$T(z_1,z_2,z_3)$ type [$\Gamma$, $z_1,z_2 \in A$, $z_3 \in \mathsf{Id}(A,z_1,z_2)$]}
    \noLine
    \UnaryInfC{$a \in A$ [$\Gamma$] $\quad$ $e(w) \in T(w,w,\mathrm{id}(w))$ [$\Gamma$, $w \in A$]}
  \LeftLabel{C-$\mathsf{Id}$)}
  \UnaryInfC{$El_{\mathsf{Id}}(\mathrm{id}(a),e) = e(a) \in T(a,a,\mathrm{id}(a))$ [$\Gamma$]}
\end{prooftree}

```agda
data Id (A : Set) : A → A → Set where
  id : (a : A) → Id A a a

ElId : {A : Set}
       {T : (z₁ z₂ : A) → Id A z₁ z₂ → Set} -- T(z₁,z₂,z₃) type [z₁,z₂ ∈ A, z₃ ∈ Id(A,z₁,z₂)]
       {a b : A} →                          -- a,b ∈ A
       (x : Id A a b) →                     -- x ∈ Id(A,a,b)
       (e : (w : A) → T w w (id w)) →       -- e(w) ∈ T(w,w,id(w)) [w ∈ A]
       T a b x                              -- ElId(x,e) ∈ T(a,b,x)
ElId (id a) e = e a                         -- ElId(id(a),e) = e(a)
```
### Strong indexed sum

The last type for which we will analyze the eliminator is the indexed sum. This type contains two values of two different types. The _pair_ (binary _tuple_) is a type, present in many programming languages, that serves this exact purpose. But the indexed sum is more powerful than a pair, indeed a pair is a special case of indexed sum.

In an indexed sum the type of the second element is a dependent type on the value of the first element. This is a bit tricky to understand, so let's see an example. Let's say the type of the first argument is a bool: we can impose that if the value of the first element is true then the second element must be a natural, and if instead it is false the second type should be unit type. If the second type is constant, we have the usual pair.

\begin{prooftree}
  \AxiomC{$C(w)$ type [$\Gamma$, $w \in B$]}
  \LeftLabel{F-$\Sigma$)}
  \UnaryInfC{$\Sigma_{w \in B}C(w)$ type [$\Gamma$]}
\end{prooftree}

The indexed type used in the example is $\Sigma_{w\in\mathsf{Bool}}C(w)$, where

$$
  C(w) = \begin{cases}
    \mathsf{Nat} & \text{if }w = \mathrm{true} \\\\
    \mathsf{Unit} & \text{if }w = \mathrm{false}
  \end{cases} = El_{\mathsf{Bool}}(w,\mathsf{Nat},\mathsf{Unit})
$$

\begin{prooftree}
  \AxiomC{$C(w)$ type [$\Gamma$, $w \in B$]}
    \AxiomC{$b \in B$ [$\Gamma$]}
      \AxiomC{$c \in C(b)$ [$\Gamma$]}
  \LeftLabel{I-$\Sigma$)}
  \TrinaryInfC{$\langle b,c \rangle\in\Sigma_{w \in B}C(w)$ [$\Gamma$]}
\end{prooftree}

The eliminators has nothing to do other than deconstructing the "pair", passing both members as parameters to a given function

$$
  El_\Sigma(x,e) = \begin{cases}
    e(b,c) & \text{if }x = \langle b, c \rangle
  \end{cases}
$$

\begin{prooftree}
  \AxiomC{$C(w)$ type [$\Gamma$, $w \in B$]}
    \noLine
    \UnaryInfC{$x \in \Sigma_{w \in B}C(w)$}
  \AxiomC{$T(z)$ type [$\Gamma$, $z \in \Sigma_{w \in B}C(w)$]}
    \noLine
    \UnaryInfC{$e(b,c) \in T(\langle b,c \rangle)$ [$\Gamma$, $b \in B$, $c \in C(b)$]}
  \LeftLabel{E-$\Sigma$)}
  \BinaryInfC{$El_\Sigma(x,e) \in T(x)$ [$\Gamma$]}
\end{prooftree}

\begin{prooftree}
  \AxiomC{$C(w)$ type [$\Gamma$, $w \in B$]}
    \noLine
    \UnaryInfC{$b \in B \quad c \in C(b)$}
  \AxiomC{$T(z)$ type [$\Gamma$, $z \in \Sigma_{w \in B}C(w)$]}
    \noLine
    \UnaryInfC{$e(b,c) \in T(\langle b,c \rangle)$ [$\Gamma$, $b \in B$, $c \in C(b)$]}
  \LeftLabel{C-$\Sigma$)}
  \BinaryInfC{$El_\Sigma(\langle b, c \rangle,e) = e(b,c) \in T(x)$ [$\Gamma$]}
\end{prooftree}
```agda
data Σ (B : Set) (C : B → Set) : Set where
  ⟨_,_⟩ : (b : B) → C b → Σ B C

ElΣ : {B : Set}
      {C : B → Set}                             -- C(w) type [w ∈ B]
      {T : Σ B C → Set} →                       -- T(z) type [z ∈ Σ(B,C(w))]
      (x : Σ B C) →                             -- x ∈ Σ(B,C(w))
      (e : (b : B) → (c : C b) → T ⟨ b , c ⟩) → -- e(b,c) ∈ T(⟨b,c⟩) [b ∈ B, c ∈ C(b)]
      T x                                       -- ElΣ(x,e) ∈ T(x)
ElΣ ⟨ b , c ⟩ e = e b c                         -- ElΣ(⟨b,c⟩,e) = e(b,c)
```

The indexed type is very useful to fix preconditions. For example we can define the positive naturals (greater than zero) as the indexed sum of naturals and a certificate of being greater than zero. We will use as certificate an identity type between $n$ (the first term of the indexed sum) and $\mathrm{suc}(m)$ for some $m \in \mathsf{Nat}$: obviously there is no such proof for $n=\mathrm{zero}$.

$$
  \mathsf{Nat}^+ = \Sigma_{n\in\mathsf{Nat}}\lambda(n).\Sigma_{m\in\mathsf{Nat}}\lambda(m).\mathsf{Id}(\mathsf{Nat}, n, \mathrm{suc}(m))
$$

which can be read as

$$
  \mathsf{Nat}^+ = \\{(n, p)\ \colon\ n \in \mathsf{Nat}, \underbrace{\exists\ m \in \mathsf{Nat}\ \colon\ n = \mathrm{suc}(m)}_{\text{proved by $p$}} \\}
$$

Now we can define a total predecessor function from $\mathsf{Nat}^+$ to $\mathsf{Nat}$.

\begin{align}
  \mathrm{pred}(x) &= \Bigl\\{\begin{cases}
      m & \text{if }p = \langle m,q \rangle,
    & \text{if }x = \langle n,p \rangle 
  \end{cases} \\\\&= El_\Sigma(x, \lambda(n,p).El_\Sigma(p, \lambda(m,q).m))
\end{align}

Recall that $p$ is $\langle m,q \rangle$ and $q$ a proof of $n = \mathrm{suc}(m)$.

```agda
Nat⁺ : Set
Nat⁺ = Σ Nat (λ n → Σ Nat (λ m → Id Nat n (suc m)))

pred : Nat⁺ → Nat
pred x = ElΣ x (λ n p → ElΣ p (λ m n≡succ[m] → m)) 
```

