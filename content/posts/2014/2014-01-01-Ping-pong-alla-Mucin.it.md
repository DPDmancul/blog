---
title: Ping pong alla Mucin
author: Davide Peressoni, Matteo Mucin, Yuri Pellegrini
cover: img/2014/Ping-pong-alla-Mucin/campo.svg
tags: [concepts]
# description: 
---

Materiale:

- 2 racchette da Ping Pong
- 1 pallina da Ping Pong

Regole:

Bisogna lanciare la pallina dall'altra parte del campo senza farla fermare. La pallina può essere presa più volte e può cadere a terra.

Se la pallina si ferma:
- **in zona A** il giocatore A perde un punto
- **in zona B** il giocatore B perde un punto
- **in zona neutra** perde un punto chi la deve ricevere

Tutti partono con 18 punti. Chi commette un fallo perde dei punti

Falli:
- lanciare la palla dalla zona neutra (in zona neutra si può solo recuperare la palla e portarla nella propria zona,senza fermarla, ma non lanciarla nella zona avversaria)  
    \-1 punto
- invadere la zona avversaria (anche zona neutra avversaria)  
    \-1 punto
- lanciare la pallina in faccia all'avversario  
    \-2 punti
- protestare con l'arbitro  
    \-2 punti
- insultare l'avversario e/o l'arbitro  
    \-3 punti
- lasciare la racchetta  
    \-8 punti
- colpire la pallina col copro volontariamente  
    \-9 punti 
