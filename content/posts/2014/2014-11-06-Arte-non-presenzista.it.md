---
title: Manifesto dell'arte non presenzista
author: Davide Peressoni, Yuri Pellegrini
# cover:
tags: [concepts]
# description:
---

  &copy; 2014 DPD- (Davide Peressoni) & Yuri Pellegrini

- L'arte non presenzista si può realizzare attraverso le più disparate tecniche (disegno, scultura, scrittura, ...).
- L'arte non presenzista ha come obiettivo rappresentare il soggetto dell'oggetto artistico in modo che non sia presente fisicamente e ognuno può immaginarselo come meglio gli pare.  
È inevitabile che le opere risulteranno tutti apparentemente uguali, ma avranno di distintivo le dimensioni, la cornice (o la base,...)  e soprattutto l'artista e il titolo: il titolo infatti è quello che dice a colui che guarda l'opera cosa rappresenta e cosa deve immaginarsi.
- La firma sull'opera non può essere apposta per motivi fisici, quindi si può apporre la firma nella targhetta descrittiva posta sulla cornice, sulla base,...
- La distinzione di un'opera dall'altra avviene attraverso i seguenti parametri:
  * Titolo
  * Tecnica
  * Dimensione
- Dato che è apparentemente facile creare opere di questa corrente, si invita gli artisti a non abusare del non presenzismo, ma bensì; a ragionare approfonditamente sulla creazione delle opere.

