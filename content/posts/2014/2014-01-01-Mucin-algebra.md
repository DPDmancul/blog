---
title: Mucin's Algebra
author: Davide Peressoni
cover: http://dakation.altervista.org/Mucin/logo.svg
tags: [concepts, math]
# description:
---

A tri-state algebra


- [constants](http://dakation.altervista.org/Mucin/page/con.php)
- [basic operations](http://dakation.altervista.org/Mucin/page/bas.php)
- [theoremes](http://dakation.altervista.org/Mucin/page/the.php)
- [detectors](http://dakation.altervista.org/Mucin/page/det.php)
- [minterms theoremes](http://dakation.altervista.org/Mucin/page/min.php)
