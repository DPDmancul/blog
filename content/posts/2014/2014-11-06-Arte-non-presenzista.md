---
title: Manifest of the not-existing art
author: Davide Peressoni, Yuri Pellegrini
# cover:
tags: [concepts]
# description:
---

  &copy; 2014 DPD- (Davide Peressoni) & Yuri Pellegrini

- Non-existent art can be realised using the most various techniques (drawing, sculpture, writing, ...).
- Non-existent art's objective is to represent the subject of the creation so that it is not physically present and everyone can imagine it as they like it.  
It is inevitable that the creations will eventually appear all the same, but they will have distinct sizes, the frame (or the base...) and especially the author and the title: the title, in fact, gives the public guidelines on what to imagine.
- Since the signature cannot be placed on the piece, due to physical motivations, it can be written on a description label put on the frame, base, ...
- Distinction between creations comes through the following parameters:
  * Title
  * Technique
  * Sizes
- Since it is apparently not so difficult to create such creations, artists are invited not to abuse of non-existent art, but to deeply reason on their creations.

