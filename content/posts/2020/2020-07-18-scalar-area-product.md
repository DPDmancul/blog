---
title: Scalar area product
author: Davide Peressoni
# cover:
tags: [math,concepts]
# description:
math: true
---

$$\newcommand\norm[1]{\left\lVert#1\right\rVert}$$

**Definition 1**. Scalar area product in ℝ<sup>3</sup>. Given
$\underline{a}, \underline{b}\in \mathbb{R}^3$
$$\underline{a}\dot\wedge\underline{b}:= \norm{\underline{a}\wedge \underline{b}}$$
Where $\underline{a}\wedge \underline{b}$ is
the vector product between the two vectors.

**Theorem 1**
$$\underline{a}\dot\wedge\underline{b}= \sqrt{\norm{\underline{a}}^2 \norm{\underline{b}}^2 - (\underline{a}\cdot\underline{b})^2}$$
Where $\underline{a}\cdot \underline{b}$ is
the usual scalar product between the two vectors.

**Proof.*
$$\underline{a}\cdot\underline{b}= \norm{\underline{a}}\norm{\underline{b}}\cos\theta \implies \cos^2\theta = \left(\frac{\underline{a}\cdot\underline{b}}{\norm{\underline{a}}\norm{\underline{b}}}\right)^2$$
$$\sin^2\theta = 1 - \cos^2\theta = 1 - \left(\frac{\underline{a}\cdot\underline{b}}{\norm{\underline{a}}\norm{\underline{b}}}\right)^2$$
$$\underline{a}\dot\wedge\underline{b}= \norm{\underline{a}\wedge \underline{b}} = \norm{\underline{a}}\norm{\underline{b}}\sin\theta =$$
$$= \norm{\underline{a}}\norm{\underline{b}}\sqrt{1-\left(\frac{\underline{a}\cdot\underline{b}}{\norm{\underline{a}}\norm{\underline{b}}}\right)^2} = \sqrt{\norm{\underline{a}}^2 \norm{\underline{b}}^2 - (\underline{a}\cdot\underline{b})^2}$$
 ◻*

**Definition 2**. General scalar area product. Given two vectors
$\underline{a}, \underline{b}\in \mathbb{R}^n$
$$\underline{a}\dot\wedge\underline{b}= \sqrt{\norm{\underline{a}}^2 \norm{\underline{b}}^2 - (\underline{a}\cdot\underline{b})^2}$$

*Remark 1*. Pythagoras relation.
$$(\underline{a}\dot\wedge\underline{b})^2 = (\norm{\underline{a}}\norm{\underline{b}})^2 - (\underline{a}\cdot\underline{b})^2$$
Since $\norm{\underline{a}}\norm{\underline{b}}$,
$\underline{a}\cdot\underline{b}$ and
$\underline{a}\dot\wedge\underline{b}$
follow the Pythagorean theorem, they form a right triangle.

*Remark 2*. Porperties.

-   $n=1 \implies a\dot\wedge b=0$
    $$a\dot\wedge b=\sqrt{\norm{a}^2 \norm{b}^2 - (a\cdot b)^2}=\sqrt{a^2 b^2 - (ab)^2} =0$$

-   Self product:
    $\underline{a}\dot\wedge\underline{a}=0$
    $$\underline{a}\dot\wedge\underline{a}=\sqrt{\norm{\underline{a}}^2 \norm{\underline{a}}^2 - (\underline{a}\cdot\underline{a})^2}=\sqrt{\norm{\underline{a}}^4 - (\norm{\underline{a}}^2)^2} =0$$

-   Commutative:
    $\underline{a}\dot\wedge\underline{b}=\underline{b}\dot\wedge\underline{a}$
    $$\underline{a}\dot\wedge\underline{b}= \sqrt{\norm{\underline{a}}^2 \norm{\underline{b}}^2 - (\underline{a}\cdot\underline{b})^2} =\sqrt{\norm{\underline{b}}^2 \norm{\underline{a}}^2 - (\underline{b}\cdot\underline{a})^2} = \underline{b}\dot\wedge\underline{a}$$

-   Multiplication by scalar:
    $(\alpha\underline{a})\dot\wedge\underline{b}= \underline{a}\dot\wedge(\alpha\underline{b}) = \alpha(\underline{a}\dot\wedge\underline{b});\quad \alpha \in \mathbb{R}$

-   Not associative because this product is not defined between a scalar
    ($\underline{a}\dot\wedge\underline{b}$)
    and a vector ($\underline{c}$).

**Theorem 2**. Distributive over addition inequality.
$$\underline{a}\dot\wedge(\underline{b}+\underline{c}) \leqslant \underline{a}\dot\wedge\underline{b}+ \underline{a}\dot\wedge\underline{c}$$

**Proof.* Vector product is distributive over addiction:
$$\underline{a}\wedge(\underline{b}+\underline{c})=\underline{a}\wedge\underline{b}+\underline{a}\wedge\underline{c}$$
so
$$\underline{a}\dot\wedge(\underline{b}+\underline{c}) = \norm{\underline{a}\wedge(\underline{b}+\underline{c})} =$$
$$= \norm{\underline{a}\wedge\underline{b}+\underline{a}\wedge\underline{c}} \leqslant \norm{\underline{a}\wedge\underline{b}}+\norm{\underline{a}\wedge\underline{c}} = \underline{a}\dot\wedge\underline{b}+ \underline{a}\dot\wedge\underline{c}$$
 ◻*

**Theorem 3**. Scalar area product as determinant.
$$\underline{a}\dot\wedge\underline{b}= \sqrt{
    \begin{vmatrix}
      \underline{a}\cdot\underline{a}& \underline{a}\cdot\underline{b}\\\\
      \underline{b}\cdot\underline{a}& \underline{b}\cdot\underline{b}
    \end{vmatrix}
  }$$

*Proof.*
$$\sqrt{
      \begin{vmatrix}
        \underline{a}\cdot\underline{a}& \underline{a}\cdot\underline{b}\\\\
        \underline{b}\cdot\underline{a}& \underline{b}\cdot\underline{b}
      \end{vmatrix}
    }
      =
    \sqrt{
      \begin{vmatrix}
        \norm{\underline{a}}^2 & \underline{a}\cdot\underline{b}\\\\
        \underline{a}\cdot\underline{b}& \norm{\underline{b}}^2
      \end{vmatrix}
    } = \sqrt{\norm{\underline{a}}^2 \norm{\underline{b}}^2 - (\underline{a}\cdot\underline{b})^2}$$
 ◻

# Generalized vector product

**Definition 3**. Norm function. Given
$\underline{x}\in \mathbb{R}^n$
ℋ<sub>*n*</sub> : ℝ<sup>*n*</sup> → ℝ<sup>+</sup>
$$\mathcal{H}\_n(\underline{x}) := \norm{\underline{x}}$$

**Definition 4**. Reverse norm function.
ℋ<sub>*n*</sub><sup>−1</sup> : ℝ<sup>+</sup> → {*B* : *B* ⊂ ℝ<sup>*n*</sup>}
with
$$\mathcal{H}\_n(\underline{x}) = x\quad \forall\underline{x}\in \mathcal{H}^{-1}\_n(x)$$
so
$$\mathcal{H}^{-1}\_n(x) = \partial B_x(\underline{0}) = \\{\underline{x}: \underline{x}\in \mathbb{R}^n \wedge \norm{\underline{x}}=x\\}$$

*Proof.*
$$\mathcal{H}\_n(\mathcal{H}^{-1}\_n(x)) = \mathcal{H}\_n(\underline{x}) = \norm{\underline{x}} =  x\quad \forall \underline{x}\in \mathcal{H}^{-1}\_n(x)$$
 ◻

*Remark 3*. Given two vectors
$\underline{a}, \underline{b}\in \mathbb{R}^3$
$$\underline{a}\wedge\underline{b}\in \mathcal{H}^{-1}\_n(\underline{a}\dot\wedge\underline{b})$$
since
$$\mathcal{H}^{-1}\_n(\underline{a}\dot\wedge\underline{b}) = \mathcal{H}^{-1}\_n(\norm{\underline{a}\wedge\underline{b}})$$

*Remark 4*. Given two vectors
$\underline{a}, \underline{b}\in \mathbb{R}^3$
$$(\underline{a}\wedge\underline{b})\cdot\underline{a}= (\underline{a}\wedge\underline{b})\cdot\underline{b}=0$$
since
$$\underline{a}\wedge\underline{b}\perp \langle\underline{a},\underline{b}\rangle$$
where
$\langle\underline{a},\underline{b}\rangle$ is
the plane spanned by the two vectors.

**Definition 5**. Generalized vector product.Given two vectors
$\underline{a}, \underline{b}\in \mathbb{R}^n$,
consistent
with *Remark 3*
and *Remark 4*, we can say that:
$$\underline{a}\wedge\underline{b}\in \mathcal{H}^{-1}\_n(\underline{a}\dot\wedge\underline{b}) \cap \langle\underline{a},\underline{b}\rangle^\perp = \\{\underline{x}: \underline{x}\in \mathbb{R}^n \wedge \norm{\underline{x}}=\underline{a}\dot\wedge\underline{b}\wedge \underline{x}\cdot\underline{a}=\underline{x}\cdot\underline{b}=0\\}$$

# Triple product

**Theorem 4**. Volume of Parallelepiped
$$V=\underline{a}\cdot(\underline{b}\wedge\underline{c}) = \norm{\underline{a}-\frac{\underline{a}\cdot\underline{b}}{\norm{\underline{b}}^2}\underline{b}-\frac{\underline{a}\cdot\underline{c}}{\norm{\underline{c}}^2}\underline{c}}(\underline{b}\dot\wedge\underline{c})$$

**Proof.*
$$V=\underline{a}\cdot(\underline{b}\wedge\underline{c}) = \norm{\underline{a}}(\underline{b}\dot\wedge\underline{c})\cos\phi$$
and
$$\norm{\underline{a}}\cos\phi = \norm{\underline{a}\_N}$$
where $\underline{a}\_N$ is the component of
$\underline{a}$ normal to the plane
$\langle\underline{b},\underline{c}\rangle$,
so
$$\norm{\underline{a}}\cos\phi = \norm{\underline{a}\_N} = \norm{\underline{a}-\underline{a}\_b-\underline{a}\_c}$$
where $\underline{a}\_x$ is the projection of
$\underline{a}$ along $\underline{x}$
$$\underline{a}\_x = \frac{\underline{a}\cdot\underline{x}}{\norm{\underline{x}}^2}\underline{x}$$
 ◻*

**Theorem 5**. Volume upper limit.
$$\|V\| \leqslant \norm{\underline{a}}(\underline{b}\dot\wedge\underline{c})$$

**Proof.*
$$\|V\|=\|\underline{a}\cdot(\underline{b}\wedge\underline{c})\| = \norm{\underline{a}}(\underline{b}\dot\wedge\underline{c})\|\cos\phi\| \leqslant \norm{\underline{a}}(\underline{b}\dot\wedge\underline{c})$$
 ◻*

*Remark 5*. Since the scalar triple product is unchanged under a
circular shift of its three operands
($\underline{a}\cdot(\underline{b}\wedge\underline{c}) = \underline{b}\cdot(\underline{c}\wedge\underline{a}) = \underline{c}\cdot(\underline{a}\wedge\underline{b})$)
$$\|V\| \leqslant \min\\{\norm{\underline{a}}(\underline{b}\dot\wedge\underline{c}), \norm{\underline{b}}(\underline{a}\dot\wedge\underline{c}), \norm{\underline{c}}(\underline{a}\dot\wedge\underline{b})\\}$$

# Calculus

**Theorem 6**. Given *f* : *D* ⊆ ℝ<sup>2</sup> → ℝ
$$\|\mathrm{D}^2 f\| = \left(\frac{\mathrm{d}}{\mathrm{d}x}\dot\wedge\frac{\mathrm{d}}{\mathrm{d}y}\right)^2f$$
where D<sup>2</sup> *f* is the Hessian matrix of *f*, and so
\|D<sup>2</sup> *f*\| is the Hessian determinant.

**Proof.*
$$\frac{\mathrm{d}}{\mathrm{d}x}\dot\wedge\frac{\mathrm{d}}{\mathrm{d}y} = \sqrt{
      \begin{vmatrix}
        \frac{\mathrm{d}^2}{\mathrm{d}x^2} & \frac{\mathrm{d}^2}{\mathrm{d}x\mathrm{d}y}\\\\
        \frac{\mathrm{d}^2}{\mathrm{d}x\mathrm{d}y} & \frac{\mathrm{d}^2}{\mathrm{d}y^2}
      \end{vmatrix}
    }$$
for **Theorem 2**
$$\left(\frac{\mathrm{d}}{\mathrm{d}x}\dot\wedge\frac{\mathrm{d}}{\mathrm{d}y}\right)^2f =
      \begin{vmatrix}
        \frac{\mathrm{d}^2f}{\mathrm{d}x^2} & \frac{\mathrm{d}^2f}{\mathrm{d}x\mathrm{d}y}\\\\
        \frac{\mathrm{d}^2f}{\mathrm{d}x\mathrm{d}y} & \frac{\mathrm{d}^2f}{\mathrm{d}y^2}
      \end{vmatrix} = \|D^2 f\|$$
 ◻*

# Scalar area product with signals

**Definition 6**. Scalar area product of signals. Given two signals
*x*, *y* ∈ ℒ<sup>2</sup>
$$x \dot\wedge y = \sqrt{\norm{x}^2 \norm{y}^2 - (x\cdot y)\overline{x\cdot y}}$$
like for vectors; where
$$x\cdot y = \mathbb{E}(x\overline y) = \int\_{-\infty}^{+\infty}x(t)\overline{y(t)} \mathrm{d}t$$
$$\norm{x^2} = x\cdot x =\mathbb{E}(x\overline x)$$

**Theorem 7**. Variance. \[Does $x\dot\wedge 1$ make
sense?\]
$$\mathrm{var}(x) = (x\dot\wedge 1)^2$$

**Proof.*
$$(x\dot\wedge 1)^2 = \norm{x}^2 \norm{1}^2 - (x\cdot1)\overline{x\cdot1} = \norm{x}^2 - (x\cdot1)\overline{x\cdot1} =$$
$$= \mathbb{E}(x\overline x) - \mathbb{E}(1x)\overline{\mathbb{E}(1x)} = \mathbb{E}(x\overline x) - \mathbb{E}(x)\overline{\mathbb{E}(x)} = \mathrm{var}(x)$$
 ◻*

**Theorem 8**. Variance limit.
$$\mathrm{var}(x+y) \leqslant \mathrm{var}(x)+\mathrm{var}(y)+2\sqrt{\mathrm{var}(x)\mathrm{var}(y)}$$
and since var(*x*+*y*) = var(*x*) + var(*y*) + 2cov(*x*,*y*)
$$\mathrm{cov}(x,y) \leqslant \sqrt{\mathrm{var}(x)\mathrm{var}(y)}$$
which is a known result.

**Proof.*
$$\mathrm{var}(x+y)=\[(x+y)\dot\wedge 1\]^2\leqslant(x\dot\wedge 1+y\dot\wedge 1)^2=\left(\sqrt{\mathrm{var}(x)}+\sqrt{\mathrm{var}(y)}\right)^2$$
 ◻*

**Theorem 9**. Scalar product with unit. \[TO CHECK\] Given
*x* ∈ ℒ<sup>2</sup>(ℝ) or *x* ∈ ℒ<sup>2</sup>(ℂ) ∧ *x* ≥ 0
$$x\cdot1 = \norm{\sqrt{x}}^2$$

*Proof.*
$$\mathbb{E}(y\overline y) = \norm{y}^2$$
*x* ⋅ 1 = 𝔼(1*x*) = 𝔼(*x*)

1.  *$x \in \mathcal{L}^2(\mathbb{R}) \implies \mathbb{E}(x) = \mathbb{E}(\sqrt{x}^2) = \mathbb{E}(\sqrt{x}\overline{\sqrt{x}})$*

2.  *$x \geqslant 0 \implies \mathbb{E}(x) = \mathbb{E}(\|x\|) = \mathbb{E}(\sqrt{x}\overline{\sqrt{x}})$*

$$\mathbb{E}(\sqrt{x}\overline{\sqrt{x}})= \norm{\sqrt{x}}^2$$
 ◻

**Theorem 10**. Scalar area product with unit. \[TO CHECK\]
$$x\dot\wedge 1 = \norm{x-\mathbb{E}(x)}$$

*Proof*.
$$\mathrm{var}(x) = \mathbb{E}((x-\mathbb{E}(x))\overline{x-\mathbb{E}(x)}) = \norm{x-\mathbb{E}(x)}^2$$
$$\mathrm{var}(x) = (x\dot\wedge 1)^2 \implies x\dot\wedge 1 = \sqrt{\mathrm{var}(x)}$$
 ◻
