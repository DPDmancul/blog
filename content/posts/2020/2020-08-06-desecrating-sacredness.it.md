---
title: Sacralità dissacrante
author: Davide Peressoni
# cover:
tags: [life, linguistics, philosophy]
description: Un viaggio alla scopertà della "sacralità", dall'etimo all'applicazione quotidiana.
---

> pubblicato su "[Nadir](https://nadir.home.blog/)" il [3 Dicembre 2020](https://web.archive.org/web/20201203110253/https://nadir.home.blog/2020/12/03/sacralita-dissacrante/)

## La maschera dell'eleganza

Quando c'è una festa o un evento importante è tradizione vestirsi eleganti. Questo gesto, volto ad addobbare anche la persona, sancisce una separazione fra i giorni ordinarî e i giorni straordinarî dando così sacralità all'occasione. _Sacro_ infatti annovera fra le sue etimologie l'accadico 𒋛𒆠𒀸, _sakāru_ che significa _sbarrare_, _separare_ (nel senso di rimarcare un confine, delineare una netta separazione); un altro etimo è _saqru_, cioè _elevare_, sacro quindi significa rimarcare la separazione tra ordinario e straordinario.

Sacro però significa anche degno di rispetto. Ma che rispetto mostreremmo nei confronti dell'ospite (oste) se mandassimo qualcun altro al posto nostro alla festa? Ebbene questo è quello che inconsciamente facciamo se ci presentiamo con un vestiario che non sentiamo nostro: stiamo indossando una maschera. Il vestito che indossiamo per tradizione, se non ci rappresenta, ci rende anonimi, ci toglie l'Io e quindi trasforma la festa straordinaria in un evento ordinario, omologo a tutti gli altri.

L'evento è reso straordinario non dagli abiti indossati, ma dall'occasione e dalle persone che vi partecipano, nella loro pienezza: è questo l'ingrediente della vera bellezza. E la bellezza ha uno stretto legame col sacro.

Fra le etimologie di _sacro_ troviamo anche l'indoeuropeo _sak_ che significa _avvicinare_. Capiamo quindi che per sacralizzare un evento dobbiamo dissacrarlo in modo da avvicinarlo a noi, al nostro essere, perché diventi parte di noi e quindi speciale. Cogliamo quindi un invito a consacrare l'ordinario che è il tempo che viviamo realmente e a far tesoro di tutti gli attimi veri della nostra vita.

## Dio: il dio che dissacra

Dividere è diabolico (infatti _Diavolo_ deriva dal greco _Διάβαλος_, _diábalos_, _colui che divide_) eppure nelle religioni solitamente sacro viene inteso nell'accezione divisoria: la sacralità separa i fedeli dalle divinità mettendoli su piani diversi. Simil modo la sacralità sancisce una separazione fra il "sacro" e il profano, cioè fra i momenti di preghiera e la vita ordinaria. Un'altra separazione è quella fra i laici (dal greco _λαός_, _laós_, _popolo_) e i sacerdoti, dal latino _sacer_ e dal protoindoeuropeo _dot_, _colui che porta al sacro_, che difatti sono una figura mediatrice sul confine del sacro, fra l'uomo e la divinità, per mezzo del sacrificio (dal latino _sacer·facere_, _rendere sacro_): l'atto che permette di superare la barriera del sacro.

Questa separazione viene spesso definita segno di rispetto, ma rispetto non vuol necessariamente dire divisione, anzi a volte chi prende le distanze è proprio chi mostra meno rispetto. Nel cristianesimo inoltre questa separazione è completamente sbagliata: oltre ad essere diabolica, Dio stesso l'ha distrutta facendosi uomo e sacrificandosi per esso. La sacralità intesa come separazione è quindi un retaggio del passato che andrebbe sostituito con una più rispettosa e, soprattutto, fruttuosa sacralità unente che avvicina il divino all'umano, il "sacro" al profano e così anche gli uomini fra loro.

## Patria: la sacralità che cancella se stessa

A braccetto con la religione, in ambito di sacralità, troviamo la patria. Anche in quest'ambito la sacralità viene spesso intesa in ambito divisorio, confondendo così la patria con i confini politici di uno stato. Ma _patria_ deriva dal latino _patrium_ [_tellus_], [_terra_] _dei padri_ (dove _terra_ non è nell'accezione di _terræ_: _terreno_, _appezzamento_, _regione_, ma di _telluris_: _mondo_, _popolazione_, _ambiente abitato_): non rappresenta quindi i nostri confini, ma la nostra tribù, la nostra comunità; la patria quindi non è altro che una grande famiglia.

Oltre a farci confondere la famiglia con dei muri, vedere la sacralità della patria nell'accezione divisoria porta a separare l'individuo dalla patria. Eleva l'ultima al di sopra del primo, proprio come avviene, nella sacralità religiosa, fra uomini e divinità. Questo a prima vista è molto corretto: mettere gli interessi della collettività al di sopra degli interessi dell'individuo è una strategia vincente per la sopravvivenza, nonché principio fondante della libertà (la mia libertà finisce dove inizia quella degli altri).

Nonostante ciò bisogna stare attenti a non far sì che la patria sia elevata a tal punto da schiacciare l'individuo. Questo accade spesso nella memoria dei caduti nelle guerre: si elevano al rango di eroi uomini che sono morti perché qualcuno, con più potere di loro, li ha mandati a uccidere e a morire nel nome della patria. Si elogia il loro sacrificio per la patria come soldati, ma così facendo si tradisce la memoria dell'uomo che era prima del soldato. L'uomo che il soldato si è portato via. Il ricordo delle gesta eroiche in battaglia cancella l'identità di una persona che era parte essenziale della patria e che ora, oltre ad essere morta per essa, viene anche dimenticata, sostituita da un'immagine preconfezionata di vittima sacrificale. Per questo, per non cancellare la patria, è necessario ricordare i caduti in guerra come le persone che erano: figli, sposi, genitori, amici, … Solo così la patria non cadrà nell'oblio di se stessa e ci farà sentire uniti, rendendosi così sacra.

## La forma di scortesia

Concludiamo questo escurso simbolico nel mondo della sacralità controversa con una rapida analisi di quella che viene definita la "forma di cortesia". Siamo abituati a "dare _del lei_" alle persone che non conosciamo o che ricoprono un ruolo superiore al nostro, come forma di rispetto. Anche qui nel rispetto di una sacralità volta a rimarcare l'importanza dell'interlocutore. Questa importanza viene rimarcata attraverso l'ormai abituale distacco dato dal _del lei_ (o dal _del voi_): questa forma mette l'interlocutore su un altro piano. Un piano estraneo a noi, appunto, dato che ci riferiamo a lui come quando parliamo di una persona estranea (_del lei_), che non rientra nella nostra cerchia (_del voi_). Ma se ci pensiamo tutto questo distacco è altamente irrispettoso: alle persone che vogliamo più bene diamo sempre _del tu_, e non sono queste le persone più importanti per noi? Ci accorgiamo allora che con la "forma di cortesia" non stiamo dando più importanza all'interlocutore ma bensì meno importanza: "Lei non è degno del mio _del tu_".
