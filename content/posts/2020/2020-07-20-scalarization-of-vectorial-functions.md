---
title: Scalarization of vectorial functions
author: Davide Peressoni
# cover:
tags: [math,concepts]
# description:
math: true
---

$$\newcommand\norm[1]{\left\lVert#1\right\rVert}$$

# Scalarization

**Definition 1**. Given
$\underline{f}: \mathbb{R}\supseteq D \rightarrow \mathbb{R}^n$
$$שׂ\underline{f}: D \rightarrow \mathbb{R}$$
$$שׂ\underline{f}(x) := \int_0^x\norm{\frac{\mathrm{d}\underline{f}(u)}{\mathrm{d}u}}\mathrm{d}u+\norm{\underline{f}(0)}$$
Where ∫<sub>0</sub><sup>*x*</sup>*g*(*u*) d*u* is the primitive
$\overline G(x)$ of *g*(*x*), with $\overline G(0) = 0$.

**Theorem 1** (Scalarization of constant). Given
$\underline{a}\in \mathbb{R}^n$
$$שׂ\underline{a}= \norm{\underline{a}}$$

Proof.
$$שׂ\underline{a}= \int_0^x\norm{\frac{\mathrm{d}\underline{a}}{\mathrm{d}u}}\mathrm{d}u+\norm{\underline{a}} = \int_0^x0\mathrm{d}u+\norm{\underline{a}} = \norm{\underline{a}}$$
 ◻

*Remark 1*. We will interpret
$$שׂ\underline{f}(\overline x) = \[שׂ(\underline{f})\](\overline x)$$
because for **Theorem1**
$$שׂ(\underline{f}(\overline x)) = \norm{\underline{f}(\overline x)}$$
isn’t very usefull.

**Theorem 2** (Derivate of $שׂ\underline{f}$).
$$שׂ'\underline{f}:= (שׂ\underline{f})' = \norm{\underline{f}'}$$

*Proof.*
$$\frac{\mathrm{d}שׂ\underline{f}(x)}{\mathrm{d}x}=\frac{\mathrm{d}\int_0^x \norm{\frac{\mathrm{d}\underline{f}(u)}{\mathrm{d}u}}\mathrm{d}u}{\mathrm{d}x}+ \cancelto{0}{\frac{\mathrm{d}\norm{\underline{f}(0)}}{\mathrm{d}x}} = \norm{\frac{\mathrm{d}\underline{f}(x)}{\mathrm{d}x}}$$
 ◻

**Theorem 3** (Second derivate of $שׂ\underline{f}$). 
$$שׂ''\underline{f}= \frac{\underline{f}'\cdot\underline{f}''}{\norm{\underline{f}'}}$$

*Proof.* For **Theorem 2**
$$\frac{\mathrm{d}^2שׂ\underline{f}(x)}{\mathrm{d}x^2}=\frac{\mathrm{d}\norm{\underline{f}'(x)}}{\mathrm{d}x} = \frac{\underline{f}'\cdot\underline{f}''}{\norm{\underline{f}'}}$$
As the derivate of $\norm{\underline{g}(x)}$ is
$$\frac{\mathrm{d}\norm{\underline{g}(x)}}{\mathrm{d}x} = \frac{\mathrm{d}\sqrt{\sum g_i(x)^2}}{\mathrm{d}x} = \frac{\mathrm{d}\sqrt{\sum g_i(x)^2}}{\mathrm{d}\sum g_i(x)^2}\frac{\mathrm{d}\sum g_i(x)^2}{\mathrm{d}x}=$$
$$= \left.\frac{\mathrm{d}\sqrt{u}}{\mathrm{d}u}\right\|\_{u=\sum g_i(x)^2}\sum\frac{\mathrm{d}g_i(x)^2}{\mathrm{d}x} = \left.\frac{1}{2\sqrt{u}}\right\|\_{u=\norm{\underline{g}(x)}^2}\sum\frac{\mathrm{d}g_i(x)^2}{\mathrm{d}g_i(x)}\frac{\mathrm{d}g_i(x)}{\mathrm{d}x}$$
$$= \frac{1}{\cancel{2}\norm{\underline{g}}}\cancel{2}\sum g_i(x) g_i'(x) = \frac{\sum g_i(x) g_i'(x)}{\norm{\underline{g}}} = \frac{\underline{g}\cdot\underline{g}'}{\norm{\underline{g}}}$$
 ◻

*Remark 2* (Second derivate of $שׂ\underline{f}$\[TO CHECK\]).
$$שׂ''\underline{f}\propto \norm{\underline{f}''}$$

*Proof.*
$$שׂ''\underline{f}= \frac{\underline{f}'\cdot\underline{f}''}{\norm{\underline{f}'}} = \frac{\cancel{\norm{\underline{f}'}}\norm{\underline{f}''}\cos(\theta)}{\cancel{\norm{\underline{f}'}}}= \norm{\underline{f}''}\cos(\theta)$$
 ◻

*Remark 3* (Derivates of $שׂ\underline{f}$).
$$שׂ'\underline{f}= \norm{\underline{f}'} =\frac{\underline{f}'\cdot\underline{f}'}{\norm{\underline{f}'}}$$
$$שׂ''\underline{f}= \frac{\underline{f}'\cdot\underline{f}''}{\norm{\underline{f}'}}$$

**Theorem 4** (Scalarization in zero).
$$שׂ\underline{f}(0) = \norm{\underline{f}(0)}$$

*Proof.*
$$שׂ\underline{f}(0) = \left.\int_0^x\norm{\frac{\mathrm{d}\underline{f}(u)}{\mathrm{d}u}}\mathrm{d}u\right\|\_{x=0}+\norm{\underline{f}(0)} = \overline G(0)+ \norm{\underline{f}(0)} = \norm{\underline{f}(0)}$$
 ◻

**Theorem 5** (Idempotence of שׂ).
$$שׂ\circשׂ= שׂ$$

*Proof.*
$$שׂשׂ\underline{f}= \int_0^x\norm{\frac{\mathrm{d}שׂ\underline{f}(u)}{\mathrm{d}u}}\mathrm{d}u + \norm{שׂ\underline{f}(0)} =$$
For **Theorem 2** and **Theorem 4**
$$= \int_0^x\norm{\norm{\frac{\mathrm{d}\underline{f}(u)}{\mathrm{d}u}}}\mathrm{d}u + \norm{\norm{\underline{f}(0)}}= \int_0^x\norm{\frac{\mathrm{d}\underline{f}(u)}{\mathrm{d}u}}\mathrm{d}u + \norm{\underline{f}(0)} = שׂ\underline{f}$$
 ◻

**Theorem 6** (Limit by norm).

1.  *x* \> 0
    $$שׂ\underline{f}(x) \geqslant \norm{\underline{f}(x)} \geqslant 0$$

2.  *x* \< 0
    $$שׂ\underline{f}(x) \leqslant \norm{\underline{f}(x)}$$

*Proof.*
$$שׂ\underline{f}= \int_0^x\norm{\underline{f}'(u)}\mathrm{d}u + \norm{\underline{f}(0)} =$$

1.  *x* \> 0
    $$\geqslant \norm{\int_0^x\underline{f}'(u)du}+\norm{\underline{f}(0)} = \norm{\left.\underline{f}(x)\right\|\_0^x}+\norm{\underline{f}(0)}=$$
    $$= \norm{\underline{f}(x)-\underline{f}(0)}+\norm{\underline{f}(0)} \geqslant \norm{\underline{f}(x)\cancel{-\underline{f}(0)}\cancel{+\underline{f}(0)}}=\norm{\underline{f}(x)}$$

2.  *x* \< 0
    $$= -\int_x^0\norm{\underline{f}'(u)}\mathrm{d}u + \norm{\underline{f}(0)}
            \leqslant -\norm{\int_x^0\underline{f}'(u)\mathrm{d}u} + \norm{\underline{f}(0)} =$$
    $$-\norm{\underline{f}(0)-\underline{f}(x)} + \norm{\underline{f}(0)} \leqslant \left\|\norm{\underline{f}(0)}-\norm{\underline{f}(0)-\underline{f}(x)}\right\|\leqslant\norm{\cancel{\underline{f}(0)}\cancel{-\underline{f}(0)}+\underline{f}(x)}$$
 ◻

**Theorem 7**.
$$שׂ\underline{f}= \norm{\underline{f}}\impliedby \exists \alpha:D\to\mathbb{R}^+\\\|\\ \underline{f}'(x) = \alpha(x)\underline{f}(x)$$

*Proof.*
$$שׂ\underline{f}= \norm{\underline{f}}\impliedby \norm{\underline{f}'} = \norm{\underline{f}}'$$
in fact
$$שׂ\underline{f}= \int_0^x \norm{\underline{f}'} du + \norm{\underline{f}(0)} = \int_0^x \norm{\underline{f}}' du + \norm{\underline{f}(0)} = \norm{\underline{f}} \cancel{- \norm{\underline{f}(0)}} \cancel{+ \norm{\underline{f}(0)}}$$
furthermore
$$\norm{\underline{f}}' = \frac{\underline{f}\cdot\underline{f}'}{\norm{\underline{f}}}$$
and so
$$\norm{\underline{f}}' = \norm{\underline{f}'} \iff \norm{\underline{f}}\norm{\underline{f}'} = \underline{f}\cdot\underline{f}' = \norm{\underline{f}}\norm{\underline{f}'}\cos(\theta)  \iff$$
 ⇔ cos (*θ*) = 1 ⇔ *θ* = 2*kπ*; *k* ∈ ℤ
and *θ* = 2*kπ*⇔ $\underline{f}$ and
$\underline{f}'$ have the same direction. ◻

**Theorem 8** (Lenght of a curve). Given a curve *Γ* ⊂ ℝ<sup>*n*</sup>
and a parametrization
$\underline{\gamma}\in C^1(\[a,b\]):\[a,b\]\to\Gamma$
$$L(\Gamma) = \left.שׂ\underline{\gamma}\right\|\_a^b$$

*Proof.*
$$שׂ\underline{\gamma}(x) = \int_0^x \norm{\underline{\gamma}'(t)}\mathrm{d}t + \norm{\underline{\gamma}(0)}$$
$$\left.שׂ\underline{\gamma}\right\|\_a^b = שׂ\underline{\gamma}(b) - שׂ\underline{\gamma}(a) = \int_0^b \norm{\underline{\gamma}'(t)}\mathrm{d}t - \int_0^a \norm{\underline{\gamma}'(t)}\mathrm{d}t =$$
$$= \int_a^0 \norm{\underline{\gamma}'(t)}\mathrm{d}t + \int_0^b \norm{\underline{\gamma}'(t)}\mathrm{d}t = \int_a^b \norm{\underline{\gamma}'(t)}\mathrm{d}t = \int\_\Gamma\mathrm{d}s = L(\Gamma)$$
 ◻

# Examples

**Example 1** (Ciruclar motion). Given *r*, *ω* ∈ ℝ<sup>+</sup>
$$\underline{f}(t) = r \left(\cos(\omega t),\sin(\omega t)\right)$$

$$\norm{\underline{f}(0)} = r$$
$$\underline{\dot f}= r\omega \left(-\sin(\omega t),\cos(\omega t)\right) \implies \norm{\underline{\dot f}}= r\omega$$
$$שׂ\underline{f}= \int_0^t\norm{\underline{\dot f}}\mathrm{d}u + \norm{\underline{f}(0)} = r(\omega t + 1)$$

And, as expected
$$\dotשׂ\underline{f}= r\omega = \norm{\underline{\dot f}}$$
