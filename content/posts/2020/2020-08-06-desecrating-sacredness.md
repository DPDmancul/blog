---
title: Desecrating sacredness
author: Davide Peressoni
# cover:
tags: [life, linguistics, philosophy]
description: A tour of the meaning of "sacredness", from the origin of the word, to our days.
---

> Translated from original article in Italian: [https://nadir.home.blog/2020/12/03/sacralita-dissacrante/](https://web.archive.org/web/20201203110253/https://nadir.home.blog/2020/12/03/sacralita-dissacrante/)

## The elegance mask

When we go to a party, or to an important event, we are used to dress elegant. This feat, intended to adorn also the person, sets off a separation between ordinary and special days, giving so sacredness to the circumstance. In facts, _sacred_ has, among its etymologies, the Akkadian 𒋛𒆠𒀸, _sakāru_ whose meaning is _to bar_, _to divide_ (in the sense of remark a border). Another etymon is _saqru_, which means _to lift up_; _sacred_ means so remarking the separation between ordinary and extraordinary.

However, sacred also means worthy of respect. And would we show respect for our host if we send somebody else, instead of us, at the party? Well, this is what we unconsciously do if we present ourselves with clothing that we don't feel ours: we are wearing a mask. The clothes we wear for tradition, if they don't represent us, make us anonymous, take away our identity and therefore transform the extraordinary party into an ordinary event, equal to all the others.

The event is made extraordinary not by the clothes worn, but by the occasion and the people who take part in it: this is the ingredient of true beauty. And beauty has a close bond with the sacred.

Among the etymologies of _sacred_ we also find the Indo-European _sak_ which means _to draw near_. We therefore understand that to sacralize an event we must desecrate it in order to draw it closer to us, to our being, so that it becomes part of us and therefore special. Let us therefore take up an invitation to consecrate the ordinary which is the time we really live and to enjoy all the true moments of our life.

## God: the god who desecrates

Dividing is diabolical (in fact _Devil_ derives from the Greek _Διάβαλος_, _diábalos_, _the one who divides_) yet in religions usually sacred it is understood in the dividing sense: sacredness separates the faithful from the divinities by placing them on different levels. Similarly, sacredness establishes a separation between the "sacred" and the profane, that is, between moments of prayer and ordinary life. Another separation is that between the laity (from the Greek _λαός_, _laós_, _people_) and the priests, from the Latin _sacer_ and the Proto-Indo-European _dot_, _the one who leads to the sacred_, who in fact are mediators on the border of the sacred, between humans and divinity, through the sacrifice (from the Latin _sacer·facere_, _to make sacred_): the act that allows to go beyond the barrier of the sacred.

This separation is often defined as a sign of respect, but respect does not necessarily mean division, indeed sometimes those who distance themselves are those who show the least respect. Furthermore, in Christianity this separation is completely wrong: in addition to being diabolical, God himself destroyed it by becoming man and sacrificing himself for humanity. Sacredness understood as separation is therefore a legacy of the past that should be replaced with a more respectful and, above all, fruitful joining sacredness that brings the divine to the human, the "sacred" to the profane and thus also men to one another.

## National pride: the sacredness that erases itself

Hand in hand with religion, in the sacredness field, we find the national pride. Also in this context, sacredness is often understood in a divisive context, thus confusing the homeland with the political borders of a state. But fatherland derives from the Latin *patrium* \[*tellus*], \[*land*] *of the fathers* (where *terra* is not in the sense of *terræ*: *land*, *plot*, *region*, but of *telluris*: *world*, *population*, *inhabited environment*): therefore it does not represent our borders, but our tribe, our community; therefore the motherland is nothing more than a large family.

In addition to making us confuse the family with walls, seeing the sacredness of the homeland in the dividing sense leads to separating the individual from the homeland. It elevates the last above the first, just as happens, in religious sacredness, between men and gods. At first sight this is very correct: putting the interests of the community above the interests of the individual is a winning strategy for survival, as well as a founding principle of freedom (my freedom ends where that of others begins).

Nevertheless, one must be careful not to let the country be elevated to such a point as to crush the individual. This often happens in the memory of the fallen in wars: men who died because someone, with more power, sent them to kill and die in the name of their nation. They are raised to the rank of heroes, their sacrifice for the nation as soldiers is praised. But in doing so, the memory of the man who was before the soldier is betrayed. The man the soldier took away. The memory of heroic deeds in battle erases the identity of a person who was an essential part of the homeland and who now, in addition to having died for it, is also forgotten, replaced by a pre-packaged image of a sacrificial victim. For this reason, in order not to erase the homeland, it is necessary to remember the war dead as the people they were: children, spouses, parents, friends, ... Only in this way the homeland won't fall into the oblivion of itself and will make us feel united, making so itself sacred.

## The (im)polite form

We conclude this symbolic excursus in the world of controversial sacredness with a quick analysis of what is defined as the "form of courtesy". In many languages we refer in a formal manner to people we don't know or who hold a higher role than ours, as a form of respect. Here too in respect of a sacredness aimed at underlining the importance of the interlocutor. The underlying of this importance, now an habitual detachment, puts the interlocutor on another level. A level foreign to us, given that we refer to him as when we speak of a foreign person, who does not fall within our circle. But if we think about it, all this detachment is highly disrespectful: we don't talk this way to the people we love most, and aren't these the most important people to us? We then realize that with the "form of courtesy" we are not giving more importance to the interlocutor but rather less importance: "You are not worthy of my informal talking".
