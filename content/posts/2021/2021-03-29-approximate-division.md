---
title: Approximate division
author: Davide Peressoni
# cover:
tags: [math, computation, estimate]
# description:
math: true
---

We will now explore some methods to approximate the
fraction
$$\frac{A}{1+B}\quad\quad\text{with } B\in(-1,1)$$

Let’s start with an iterative algorithm which starts from *A − AB*
and at each iteration obtains a closer value to the desired result:

> **Iterative algorithm to approximate $\frac{A}{1+B}$**\
> **Input:** $A; B\in(-1,1)$\
> **Output:** $x\approx\frac{A}{1+B}$\
> $x = A$;\
> **until** $x$ has a stable enough value **do**\
> | &emsp; $x \leftarrow A - Bx$;\
> **done**\
> **return** $x$;

We can describe the algorithm also in a recursive way:
\begin{aligned}
  f(x) &= A - Bx\\\\
  \frac{A}{1+B} &\approx f^n(A) = \underbrace{f\left(f\left(\dots f(A))\right)\right)}\_n\quad n\text{ big enough}
\end{aligned}

**Lemma 1** (First step error). *The relative error of the first step is
*B*<sup>2</sup>.*

*Proof.* The absolute error is
\begin{aligned}
    \varepsilon &= \left\lvert f(A) - \frac{A}{1+B}\right\rvert = \left\lvert A - AB - \frac{A}{1+B}\right\rvert = \left\lvert \frac{AB}{1+B} - AB\right\rvert =\\\\
    &= \frac{\left\lvert \cancel{AB} - \cancel{AB} - AB^2\right\rvert}{1+B} = \frac{\lvert A\rvert}{1+B} B^2
\end{aligned}
and so the relative error is
$$\eta = \left\lvert \frac\varepsilon{\frac{A}{1+B}}\right\rvert = B^2$$ ◻

*Remark 1*. Being *B* ∈ (−1,1) then *B*<sup>2</sup> \< \|*B*\|, so for
small values of \|*B*\| the first step could be a good approximation.

**Theorem 1** (Algorihtm convergence). *At each application of *f* the
result is closer to $\frac{A}{1+B}$, and so the previous algorithm
converges to the desired result.*

*Proof.* Given $\varepsilon = f(x) - \frac{A}{1+B}$, the error of the
previous application of *f*, we can see that the error of this
application is:
\begin{aligned}
    \varepsilon' &= f(f(x)) - \frac{A}{1+B} = A - Bf(x) - \frac{A}{1+B} = \frac{\cancel{A} + AB \cancel{-A}}{1+B} - Bf(x) =\\\\
    &= \frac{AB}{1+B} - B \left(\varepsilon + \frac{A}{1+B}\right) = \cancel{\frac{AB}{1+B}} - B\varepsilon - \cancel{B\frac{A}{1+B}} = -B\varepsilon
\end{aligned}
and since *B* ∈ (−1,1) then \|*ε*′\| \< \|*ε*\| ◻

**Corollary 1** (Relative erorror). *The relative error of
*f*<sup>*n*</sup>(*A*) is
$\lvert B^{n+1}\rvert\left(\frac{1+B}{A}\right)^{2(n-1)}$ for *n* ≥ 1.*

*Proof.* We will prove it by induction:

-   *n* = 1; from **Lemma 1** the relative
    error is
    *η* = *B*<sup>2</sup>
    and
    $$\lvert B^{n+1}\rvert\left(\frac{1+B}{A}\right)^{2(n-1)} = \lvert B^2\rvert\left(\frac{1+B}{A}\right)^0=B^2$$

-   From **Theorem 1** we know that:
    *ε*′ =  − *Bε*
    and so the relative error is:
    $$\eta' = \left\lvert\frac{-B\varepsilon}{\frac{A}{1+B}} \right\rvert  = \left\lvert\frac{-B\eta\frac{1+B}{A}}{\frac{A}{1+B}} \right\rvert = \lvert B\eta\rvert \frac{(1+B)^2}{A^2}$$

 ◻

**Theorem 2** (Another way for big *B*). *The relative error of the mean
of the first two steps $\frac{f(A)+f^2(A)}2$ is
$\frac{\lvert B-1\rvert}2 B^2$.*

*Proof.*
\begin{aligned}
    \frac{f(A)+f^2(A)}2 &= \frac{(A-AB) + (A-(A-AB)B)}2 = \frac{A-AB+A-AB+AB^2}2=\\\\
    &= \frac{2A-2AB+AB^2}2= A-AB +\frac{A}2B^2
\end{aligned}
so the absolute error is
\begin{aligned}
    \varepsilon &= \left\lvert \frac{f(A)+f^2(A)}2 - \frac{A}{1+B}\right\rvert = \left\lvert A-AB +\frac{A}2B^2 - \frac{A}{1+B}\right\rvert \\\\
    &= \left\lvert \frac{(A-AB)(1+B)-A}{1+B} +\frac{A}2B^2 -\right\rvert = \left\lvert \frac{\cancel{A}+\cancel{AB}\cancel{-AB}-AB^2\cancel{-A}}{1+B} +\frac{A}2B^2 \right\rvert \\\\
    &= \left\lvert \frac{AB^2}2 - \frac{AB^2}{1+B} \right\rvert = \left\lvert \frac{A}{1+B}\frac{1+B-2}2\right\rvert B^2 = \left\lvert \frac{A}{1+B}\frac{B-1}2\right\rvert B^2
\end{aligned}
and the relative error
\begin{aligned}
    \eta &= \frac{\lvert B-1\rvert}2 B^2
\end{aligned}
 ◻

*Remark 2*. For *B* ≈ 1 approximating $\frac{A}{1+B}$ with
$\frac{f(A)+f^2(A)}2$ is better than with *f*<sup>2</sup>(*A*).

*Proof.*
\begin{aligned}
    \frac{\lvert B-1\rvert}2 B^2 \< \lvert B^3\rvert\left(\frac{1+B}{A}\right)^2 &\Leftrightarrow A^2\lvert B-1\rvert\< 2\lvert B\rvert(1+B)^2 \< 2\cdot1(1+1)^2 = 8 \implies\\\\
    &\implies \lvert B-1\rvert \< \frac8{A^2} \Leftrightarrow B\in1\pm\frac8{A^2} \approx 1
\end{aligned}
 ◻
