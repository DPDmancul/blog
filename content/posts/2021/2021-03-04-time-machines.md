---
title: Regard time machines
author: Davide Peressoni
# cover:
tags: [probability, philosophy, logic, estimate]
description: Will time machines be build? The probability that in future a time machine that can travel to the past would be build is very low. 
toc: false
---

# Theorem
## Will time machines be build?

The probability that in future a time machine that can travel to the past would be build is very low.

## Proof

Suppose by contradiction a time machine which can travel to the past will be build some day. Then it is logical to suppose the machine will be discovered by the people soon or later, in spite of all preventive measures to keep it secret.

If the machine would be discovered by people it will become mainstream. Then it is obvious that at least one person would think to go to the past to sell something we, people of the past, would pay a lot to have. Then in years the number of the people doing this will increase. The more the people will travel to the past the more the probabilities of been discovered will be high. 

Hence every year the probability of discovering someone from the future is increasing, thus, since we actually don't know any similar case, the objective probability is decreasing.

## Remark

The above theorem does not state that a time machine is infeasible, but only that it will never be build, with high probability. The motivation of this could be something different from the possible infeasibility, for example this theorem could be the cause: since this theorem demonstrate that nobody will likely build a time machine one could legally think investing in time machine research is not worth of it.
