---
title: Vectorial complex division
author: Davide Peressoni
# cover:
tags: [math,concepts]
# description:
math: true
---

$$\newcommand\norm[1]{\left\lVert#1\right\rVert}$$

Given *v⃗*, *u⃗* ∈ ℝ<sup>*n*</sup> with $\vec{v}=\norm{\vec{v}}\hat{v}$
and $\vec{u}=\norm{\vec{u}}\hat{u}$, we can write
$$\ln\vec{v}=\ln\left(\norm{\vec{v}}\hat{v}\right)=\ln\norm{v}+\ln\hat{v}$$

Now let’s take the plane spanned by the two vectors: *S* = ⟨*v⃗*, *u⃗*⟩.
In this plane we can fix a Gauß plane in order to calculate ln *v̂*:
ln *v̂* = (∠*v̂*+*γ*)*i*
where *γ* ∈ ℝ is an arbitrary constant, derived from the fact we cannot
know the real phase of *v̂* in the Gauß plane, since it depends on how we
fixed the axis.

We cannot compute the phase of *v̂* but we can easily compute the
difference of phase of two versors: it is the angle between the two
$$(\angle{\hat{v}}+\cancel\gamma)-(\angle{\hat{u}}+\cancel\gamma) = \angle{\hat{v}}-\angle{\hat{u}}=\widehat{\hat{v}\hat{u}}$$
and from the formula of dot product:
$$\hat{v}\cdot\hat{u}=\norm{\hat{v}}\norm{\hat{u}}\cos\left(\widehat{\hat{v}\hat{u}}\right)$$
$$\widehat{\hat{v}\hat{u}}= \pm\cos^{-1}\frac{\hat{v}\cdot\hat{u}}{\norm{\hat{v}}\norm{\hat{u}}}= \pm\cos^{-1}\left(\hat{v}\cdot\hat{u}\right)$$

Finally we can compute the complex division of the two vectors, by means
of its logarithm:
$$\begin{aligned}
  \ln\frac{\vec{v}}{\vec{u}}
  &= \ln\norm{\vec{v}}+\ln\hat{v} - \left(\ln\norm{\vec{u}}+\ln\hat{u}\right) =
    \ln\frac{\norm{\vec{v}}}{\norm{\vec{u}}} + \left(\ln\hat{v}-\ln\hat{u}\right) =\\\\
  &= \ln\frac{\norm{\vec{v}}}{\norm{\vec{u}}} \pm \cos^{-1}\left(\hat{v}\cdot\hat{u}\right)i
\end{aligned}$$
and
$$\begin{aligned}
  \frac{\vec{v}}{\vec{u}}
  &=e^{\ln\frac{\norm{\vec{v}}}{\norm{\vec{u}}} \pm \cos^{-1}\left(\hat{v}\cdot\hat{u}\right)i}=\frac{\norm{\vec{v}}}{\norm{\vec{u}}}e^{\pm \cos^{-1}\left(\hat{v}\cdot\hat{u}\right)i}=\\\\
  &=\frac{\norm{\vec{v}}}{\norm{\vec{u}}}
    \left(\cos\left(\pm\cos^{-1}\left(\hat{v}\cdot\hat{u}\right)\right)+\sin\left(\pm\cos^{-1}\left(\hat{v}\cdot\hat{u}\right)\right)i\right)=\\\\
  &=\frac{\norm{\vec{v}}}{\norm{\vec{u}}}\left(\hat{v}\cdot\hat{u}\pm\sqrt{1-\left(\hat{v}\cdot\hat{u}\right)^{2}}i\right)
\end{aligned}$$

For the last step was used the following identity:
$$\sin\cos^{-1}x = \sqrt{\sin^{2}\cos^{-1}x}=\sqrt{1-\cos^{2}\cos^{-1}x}=\sqrt{1-x^{2}}$$

# Properties

## Division of a vector by itself  
$$\frac{\vec{v}}{\vec{v}}= \cancel{\frac{\norm{\vec{v}}}{\norm{\vec{v}}}}\left(\hat{v}\cdot\hat{v}\pm\sqrt{1-\left(\hat{v}\cdot\hat{v}\right)^{2}}i\right)$$
and since $\hat{v}\cdot\hat{v}=\norm{\hat{v}}^{2}=1$
$$\frac{\vec{v}}{\vec{v}}=1\pm\sqrt{1-1}i=1$$

## Division by identity  
There is no unity vector, but all versors are unitar:
$$\frac{\vec{v}}{\hat{u}}= \frac{\norm{\vec{v}}}{\cancel{\norm{\hat{u}}}}\left(\hat{v}\cdot\hat{u}\pm\sqrt{1-\left(\hat{v}\cdot\hat{u}\right)^{2}}i\right)$$
we don’t know the value of *v̂* ⋅ *û*, but we can compute the module of
the division:
$$\left\lvert\frac{\vec{v}}{\hat{u}}\right\rvert= \norm{\vec{v}} \left(\cancel{\left(\hat{v}\cdot\hat{u}\right)^{2}}+1-\cancel{\left(\hat{v}\cdot\hat{u}\right)^{2}}\right)=\norm{\vec{v}}$$
So the module is the same of the magnitude of *v⃗*, as expected. As for
the direction, it is simple to observe that for each choice of the
Gauß plane axis, there exist a versor for which the direction is the
same of *v⃗*.

## Inverse  
It’s simple to observe as swapping numerator and denominator makes the
module inverse and doesn’t affect the phase. So
$$\left\lvert\frac{\hat{u}}{\vec{v}}\right\rvert=\frac1{\norm{\hat{v}}}$$

